package com.telkom.aplikasiku.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ilhamsaputra on 12/7/17.
 */

public class ModelDataVendorDetail {
    @SerializedName("vendor_detail")
    @Expose
    private List<ModelVendorDetail> vendorDetail = new ArrayList<ModelVendorDetail>();

    public List<ModelVendorDetail> getVendorDetail() {
        return vendorDetail;
    }

    public void setVendorDetail(List<ModelVendorDetail> vendorDetail) {
        this.vendorDetail = vendorDetail;
    }
}
