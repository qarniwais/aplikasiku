package com.telkom.aplikasiku.model;

/**
 * Created by HP on 12/5/2017.
 */

public class ModelLogin {

    private String user_login;
    private String user_id;
    private String username;
    private String surname;
    private String email;
    private String phone;

    public ModelLogin(){

    }

    public ModelLogin(String user_login, String user_id, String username, String surname, String email, String phone) {
        this.user_login = user_login;
        this.user_id = user_id;
        this.username = username;
        this.surname = surname;
        this.email = email;
        this.phone = phone;
    }

    public String getUser_login() {
        return user_login;
    }

    public void setUser_login(String user_login) {
        this.user_login = user_login;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
