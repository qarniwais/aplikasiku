package com.telkom.aplikasiku.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ilhamsaputra on 12/7/17.
 */

public class ModelVendorDetail {
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("surname")
    @Expose
    private String surname;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("address1")
    @Expose
    private Object address1;
    @SerializedName("address2")
    @Expose
    private Object address2;
    @SerializedName("city")
    @Expose
    private Object city;
    @SerializedName("zip")
    @Expose
    private Object zip;
    @SerializedName("langlat")
    @Expose
    private String langlat;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("fb_id")
    @Expose
    private Object fbId;
    @SerializedName("g_id")
    @Expose
    private Object gId;
    @SerializedName("g_photo")
    @Expose
    private Object gPhoto;
    @SerializedName("creation_date")
    @Expose
    private String creationDate;
    @SerializedName("google_plus")
    @Expose
    private Object googlePlus;
    @SerializedName("skype")
    @Expose
    private Object skype;
    @SerializedName("facebook")
    @Expose
    private Object facebook;
    @SerializedName("wishlist")
    @Expose
    private String wishlist;
    @SerializedName("last_login")
    @Expose
    private String lastLogin;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("user_type_till")
    @Expose
    private Object userTypeTill;
    @SerializedName("left_product_type")
    @Expose
    private String leftProductType;
    @SerializedName("downloads")
    @Expose
    private String downloads;
    @SerializedName("country")
    @Expose
    private Object country;
    @SerializedName("state")
    @Expose
    private Object state;
    @SerializedName("subscribe")
    @Expose
    private Object subscribe;
    @SerializedName("privy_id")
    @Expose
    private Object privyId;
    @SerializedName("tmoney_id")
    @Expose
    private String tmoneyId;
    @SerializedName("verification_hash")
    @Expose
    private Object verificationHash;
    @SerializedName("tmoney_detail")
    @Expose
    private String tmoneyDetail;
    @SerializedName("tmoney_activation_response")
    @Expose
    private String tmoneyActivationResponse;
    @SerializedName("tmoney_password")
    @Expose
    private String tmoneyPassword;
    @SerializedName("pks_sign")
    @Expose
    private String pksSign;
    @SerializedName("privy_detail")
    @Expose
    private Object privyDetail;
    @SerializedName("privy_otp")
    @Expose
    private Object privyOtp;
    @SerializedName("ktp")
    @Expose
    private Object ktp;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Object getAddress1() {
        return address1;
    }

    public void setAddress1(Object address1) {
        this.address1 = address1;
    }

    public Object getAddress2() {
        return address2;
    }

    public void setAddress2(Object address2) {
        this.address2 = address2;
    }

    public Object getCity() {
        return city;
    }

    public void setCity(Object city) {
        this.city = city;
    }

    public Object getZip() {
        return zip;
    }

    public void setZip(Object zip) {
        this.zip = zip;
    }

    public String getLanglat() {
        return langlat;
    }

    public void setLanglat(String langlat) {
        this.langlat = langlat;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Object getFbId() {
        return fbId;
    }

    public void setFbId(Object fbId) {
        this.fbId = fbId;
    }

    public Object getGId() {
        return gId;
    }

    public void setGId(Object gId) {
        this.gId = gId;
    }

    public Object getGPhoto() {
        return gPhoto;
    }

    public void setGPhoto(Object gPhoto) {
        this.gPhoto = gPhoto;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public Object getGooglePlus() {
        return googlePlus;
    }

    public void setGooglePlus(Object googlePlus) {
        this.googlePlus = googlePlus;
    }

    public Object getSkype() {
        return skype;
    }

    public void setSkype(Object skype) {
        this.skype = skype;
    }

    public Object getFacebook() {
        return facebook;
    }

    public void setFacebook(Object facebook) {
        this.facebook = facebook;
    }

    public String getWishlist() {
        return wishlist;
    }

    public void setWishlist(String wishlist) {
        this.wishlist = wishlist;
    }

    public String getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public Object getUserTypeTill() {
        return userTypeTill;
    }

    public void setUserTypeTill(Object userTypeTill) {
        this.userTypeTill = userTypeTill;
    }

    public String getLeftProductType() {
        return leftProductType;
    }

    public void setLeftProductType(String leftProductType) {
        this.leftProductType = leftProductType;
    }

    public String getDownloads() {
        return downloads;
    }

    public void setDownloads(String downloads) {
        this.downloads = downloads;
    }

    public Object getCountry() {
        return country;
    }

    public void setCountry(Object country) {
        this.country = country;
    }

    public Object getState() {
        return state;
    }

    public void setState(Object state) {
        this.state = state;
    }

    public Object getSubscribe() {
        return subscribe;
    }

    public void setSubscribe(Object subscribe) {
        this.subscribe = subscribe;
    }

    public Object getPrivyId() {
        return privyId;
    }

    public void setPrivyId(Object privyId) {
        this.privyId = privyId;
    }

    public String getTmoneyId() {
        return tmoneyId;
    }

    public void setTmoneyId(String tmoneyId) {
        this.tmoneyId = tmoneyId;
    }

    public Object getVerificationHash() {
        return verificationHash;
    }

    public void setVerificationHash(Object verificationHash) {
        this.verificationHash = verificationHash;
    }

    public String getTmoneyDetail() {
        return tmoneyDetail;
    }

    public void setTmoneyDetail(String tmoneyDetail) {
        this.tmoneyDetail = tmoneyDetail;
    }

    public String getTmoneyActivationResponse() {
        return tmoneyActivationResponse;
    }

    public void setTmoneyActivationResponse(String tmoneyActivationResponse) {
        this.tmoneyActivationResponse = tmoneyActivationResponse;
    }

    public String getTmoneyPassword() {
        return tmoneyPassword;
    }

    public void setTmoneyPassword(String tmoneyPassword) {
        this.tmoneyPassword = tmoneyPassword;
    }

    public String getPksSign() {
        return pksSign;
    }

    public void setPksSign(String pksSign) {
        this.pksSign = pksSign;
    }

    public Object getPrivyDetail() {
        return privyDetail;
    }

    public void setPrivyDetail(Object privyDetail) {
        this.privyDetail = privyDetail;
    }

    public Object getPrivyOtp() {
        return privyOtp;
    }

    public void setPrivyOtp(Object privyOtp) {
        this.privyOtp = privyOtp;
    }

    public Object getKtp() {
        return ktp;
    }

    public void setKtp(Object ktp) {
        this.ktp = ktp;
    }
}
