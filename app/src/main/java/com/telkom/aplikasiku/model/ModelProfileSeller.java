package com.telkom.aplikasiku.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by HP on 12/6/2017.
 */

public class ModelProfileSeller {
    @SerializedName("vendor_id")
    @Expose
    private String vendorId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("company")
    @Expose
    private Object company;
    @SerializedName("display_name")
    @Expose
    private String displayName;
    @SerializedName("address1")
    @Expose
    private Object address1;
    @SerializedName("address2")
    @Expose
    private Object address2;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("membership")
    @Expose
    private String membership;
    @SerializedName("create_timestamp")
    @Expose
    private String createTimestamp;
    @SerializedName("approve_timestamp")
    @Expose
    private String approveTimestamp;
    @SerializedName("member_timestamp")
    @Expose
    private Object memberTimestamp;
    @SerializedName("member_expire_timestamp")
    @Expose
    private Object memberExpireTimestamp;
    @SerializedName("details")
    @Expose
    private Object details;
    @SerializedName("last_login")
    @Expose
    private Object lastLogin;
    @SerializedName("facebook")
    @Expose
    private Object facebook;
    @SerializedName("skype")
    @Expose
    private Object skype;
    @SerializedName("google_plus")
    @Expose
    private Object googlePlus;
    @SerializedName("twitter")
    @Expose
    private Object twitter;
    @SerializedName("youtube")
    @Expose
    private Object youtube;
    @SerializedName("pinterest")
    @Expose
    private Object pinterest;
    @SerializedName("stripe_details")
    @Expose
    private Object stripeDetails;
    @SerializedName("paypal_email")
    @Expose
    private Object paypalEmail;
    @SerializedName("preferred_payment")
    @Expose
    private Object preferredPayment;
    @SerializedName("cash_set")
    @Expose
    private Object cashSet;
    @SerializedName("stripe_set")
    @Expose
    private Object stripeSet;
    @SerializedName("paypal_set")
    @Expose
    private Object paypalSet;
    @SerializedName("phone")
    @Expose
    private Object phone;
    @SerializedName("keywords")
    @Expose
    private Object keywords;
    @SerializedName("description")
    @Expose
    private Object description;
    @SerializedName("lat_lang")
    @Expose
    private String latLang;
    @SerializedName("country")
    @Expose
    private Object country;
    @SerializedName("city")
    @Expose
    private Object city;
    @SerializedName("zip")
    @Expose
    private Object zip;
    @SerializedName("state")
    @Expose
    private Object state;
    @SerializedName("c2_set")
    @Expose
    private Object c2Set;
    @SerializedName("c2_user")
    @Expose
    private Object c2User;
    @SerializedName("c2_secret")
    @Expose
    private Object c2Secret;
    @SerializedName("endpoint")
    @Expose
    private Object endpoint;
    @SerializedName("avatar")
    @Expose
    private Object avatar;
    @SerializedName("website")
    @Expose
    private Object website;
    @SerializedName("facebook_id")
    @Expose
    private Object facebookId;
    @SerializedName("google_id")
    @Expose
    private String googleId;
    @SerializedName("ktp")
    @Expose
    private Object ktp;
    @SerializedName("post_response")
    @Expose
    private Object postResponse;
    @SerializedName("privy_id")
    @Expose
    private Object privyId;
    @SerializedName("tmoney_id")
    @Expose
    private Object tmoneyId;
    @SerializedName("verification_hash")
    @Expose
    private Object verificationHash;
    @SerializedName("tmoney_detail")
    @Expose
    private Object tmoneyDetail;
    @SerializedName("tmoney_activation_response")
    @Expose
    private Object tmoneyActivationResponse;
    @SerializedName("tmoney_password")
    @Expose
    private Object tmoneyPassword;
    @SerializedName("privy_detail")
    @Expose
    private Object privyDetail;
    @SerializedName("privy_otp")
    @Expose
    private Object privyOtp;

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Object getCompany() {
        return company;
    }

    public void setCompany(Object company) {
        this.company = company;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Object getAddress1() {
        return address1;
    }

    public void setAddress1(Object address1) {
        this.address1 = address1;
    }

    public Object getAddress2() {
        return address2;
    }

    public void setAddress2(Object address2) {
        this.address2 = address2;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMembership() {
        return membership;
    }

    public void setMembership(String membership) {
        this.membership = membership;
    }

    public String getCreateTimestamp() {
        return createTimestamp;
    }

    public void setCreateTimestamp(String createTimestamp) {
        this.createTimestamp = createTimestamp;
    }

    public String getApproveTimestamp() {
        return approveTimestamp;
    }

    public void setApproveTimestamp(String approveTimestamp) {
        this.approveTimestamp = approveTimestamp;
    }

    public Object getMemberTimestamp() {
        return memberTimestamp;
    }

    public void setMemberTimestamp(Object memberTimestamp) {
        this.memberTimestamp = memberTimestamp;
    }

    public Object getMemberExpireTimestamp() {
        return memberExpireTimestamp;
    }

    public void setMemberExpireTimestamp(Object memberExpireTimestamp) {
        this.memberExpireTimestamp = memberExpireTimestamp;
    }

    public Object getDetails() {
        return details;
    }

    public void setDetails(Object details) {
        this.details = details;
    }

    public Object getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Object lastLogin) {
        this.lastLogin = lastLogin;
    }

    public Object getFacebook() {
        return facebook;
    }

    public void setFacebook(Object facebook) {
        this.facebook = facebook;
    }

    public Object getSkype() {
        return skype;
    }

    public void setSkype(Object skype) {
        this.skype = skype;
    }

    public Object getGooglePlus() {
        return googlePlus;
    }

    public void setGooglePlus(Object googlePlus) {
        this.googlePlus = googlePlus;
    }

    public Object getTwitter() {
        return twitter;
    }

    public void setTwitter(Object twitter) {
        this.twitter = twitter;
    }

    public Object getYoutube() {
        return youtube;
    }

    public void setYoutube(Object youtube) {
        this.youtube = youtube;
    }

    public Object getPinterest() {
        return pinterest;
    }

    public void setPinterest(Object pinterest) {
        this.pinterest = pinterest;
    }

    public Object getStripeDetails() {
        return stripeDetails;
    }

    public void setStripeDetails(Object stripeDetails) {
        this.stripeDetails = stripeDetails;
    }

    public Object getPaypalEmail() {
        return paypalEmail;
    }

    public void setPaypalEmail(Object paypalEmail) {
        this.paypalEmail = paypalEmail;
    }

    public Object getPreferredPayment() {
        return preferredPayment;
    }

    public void setPreferredPayment(Object preferredPayment) {
        this.preferredPayment = preferredPayment;
    }

    public Object getCashSet() {
        return cashSet;
    }

    public void setCashSet(Object cashSet) {
        this.cashSet = cashSet;
    }

    public Object getStripeSet() {
        return stripeSet;
    }

    public void setStripeSet(Object stripeSet) {
        this.stripeSet = stripeSet;
    }

    public Object getPaypalSet() {
        return paypalSet;
    }

    public void setPaypalSet(Object paypalSet) {
        this.paypalSet = paypalSet;
    }

    public Object getPhone() {
        return phone;
    }

    public void setPhone(Object phone) {
        this.phone = phone;
    }

    public Object getKeywords() {
        return keywords;
    }

    public void setKeywords(Object keywords) {
        this.keywords = keywords;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public String getLatLang() {
        return latLang;
    }

    public void setLatLang(String latLang) {
        this.latLang = latLang;
    }

    public Object getCountry() {
        return country;
    }

    public void setCountry(Object country) {
        this.country = country;
    }

    public Object getCity() {
        return city;
    }

    public void setCity(Object city) {
        this.city = city;
    }

    public Object getZip() {
        return zip;
    }

    public void setZip(Object zip) {
        this.zip = zip;
    }

    public Object getState() {
        return state;
    }

    public void setState(Object state) {
        this.state = state;
    }

    public Object getC2Set() {
        return c2Set;
    }

    public void setC2Set(Object c2Set) {
        this.c2Set = c2Set;
    }

    public Object getC2User() {
        return c2User;
    }

    public void setC2User(Object c2User) {
        this.c2User = c2User;
    }

    public Object getC2Secret() {
        return c2Secret;
    }

    public void setC2Secret(Object c2Secret) {
        this.c2Secret = c2Secret;
    }

    public Object getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(Object endpoint) {
        this.endpoint = endpoint;
    }

    public Object getAvatar() {
        return avatar;
    }

    public void setAvatar(Object avatar) {
        this.avatar = avatar;
    }

    public Object getWebsite() {
        return website;
    }

    public void setWebsite(Object website) {
        this.website = website;
    }

    public Object getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(Object facebookId) {
        this.facebookId = facebookId;
    }

    public String getGoogleId() {
        return googleId;
    }

    public void setGoogleId(String googleId) {
        this.googleId = googleId;
    }

    public Object getKtp() {
        return ktp;
    }

    public void setKtp(Object ktp) {
        this.ktp = ktp;
    }

    public Object getPostResponse() {
        return postResponse;
    }

    public void setPostResponse(Object postResponse) {
        this.postResponse = postResponse;
    }

    public Object getPrivyId() {
        return privyId;
    }

    public void setPrivyId(Object privyId) {
        this.privyId = privyId;
    }

    public Object getTmoneyId() {
        return tmoneyId;
    }

    public void setTmoneyId(Object tmoneyId) {
        this.tmoneyId = tmoneyId;
    }

    public Object getVerificationHash() {
        return verificationHash;
    }

    public void setVerificationHash(Object verificationHash) {
        this.verificationHash = verificationHash;
    }

    public Object getTmoneyDetail() {
        return tmoneyDetail;
    }

    public void setTmoneyDetail(Object tmoneyDetail) {
        this.tmoneyDetail = tmoneyDetail;
    }

    public Object getTmoneyActivationResponse() {
        return tmoneyActivationResponse;
    }

    public void setTmoneyActivationResponse(Object tmoneyActivationResponse) {
        this.tmoneyActivationResponse = tmoneyActivationResponse;
    }

    public Object getTmoneyPassword() {
        return tmoneyPassword;
    }

    public void setTmoneyPassword(Object tmoneyPassword) {
        this.tmoneyPassword = tmoneyPassword;
    }

    public Object getPrivyDetail() {
        return privyDetail;
    }

    public void setPrivyDetail(Object privyDetail) {
        this.privyDetail = privyDetail;
    }

    public Object getPrivyOtp() {
        return privyOtp;
    }

    public void setPrivyOtp(Object privyOtp) {
        this.privyOtp = privyOtp;
    }
}
