package com.telkom.aplikasiku.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by trisa on 08/12/2017.
 */

public class JSONDetailProduct {
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("os_version")
    @Expose
    private String osVersion;
    @SerializedName("hp_type")
    @Expose
    private String hpType;
    @SerializedName("user_id")
    @Expose
    private String userId;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    public String getHpType() {
        return hpType;
    }

    public void setHpType(String hpType) {
        this.hpType = hpType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
