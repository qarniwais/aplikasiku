package com.telkom.aplikasiku.model;

/**
 * Created by HP on 12/4/2017.
 */

public class ModelListProduk {

    private String main_photos;
    private String title;
    private String rating_total;
    private String sale_price;
    private String purchase_price;
    private String description;
    private String referal_fee;
    private String product_id;
    private String discount_type;

    public ModelListProduk(){}

    public ModelListProduk(String main_photos, String title, String rating_total, String sale_price, String purchase_price, String description, String referal_fee, String product_id, String discount_type) {
        this.main_photos = main_photos;
        this.title = title;
        this.rating_total = rating_total;
        this.sale_price = sale_price;
        this.purchase_price = purchase_price;
        this.description = description;
        this.referal_fee = referal_fee;
        this.product_id = product_id;
        this.discount_type = discount_type;
    }

    public String getMain_photos() {
        return main_photos;
    }

    public void setMain_photos(String main_photos) {
        this.main_photos = main_photos;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRating_total() {
        return rating_total;
    }

    public void setRating_total(String rating_total) {
        this.rating_total = rating_total;
    }

    public String getSale_price() {
        return sale_price;
    }

    public void setSale_price(String sale_price) {
        this.sale_price = sale_price;
    }

    public String getPurchase_price() {
        return purchase_price;
    }

    public void setPurchase_price(String purchase_price) {
        this.purchase_price = purchase_price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getReferal_fee() {
        return referal_fee;
    }

    public void setReferal_fee(String referal_fee) {
        this.referal_fee = referal_fee;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getDiscount_type() {
        return discount_type;
    }

    public void setDiscount_type(String discount_type) {
        this.discount_type = discount_type;
    }
}
