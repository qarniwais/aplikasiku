package com.telkom.aplikasiku.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by trisa on 08/12/2017.
 */

public class ModelReferalUser {
    @SerializedName("referral_url")
    @Expose
    private String referralUrl;

    public String getReferralUrl() {
        return referralUrl;
    }

    public void setReferralUrl(String referralUrl) {
        this.referralUrl = referralUrl;
    }
}
