package com.telkom.aplikasiku.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by trisa on 08/12/2017.
 */

public class ModelDataProductDetail {
    @SerializedName("product_details")
    @Expose
    private List<ModelProductDetail> productDetails = null;
    @SerializedName("profil_seller")
    @Expose
    private ModelProfileSeller profilSeller;
    @SerializedName("product_tags")
    @Expose
    private String productTags;
    @SerializedName("user")
    @Expose
    private ModelReferalUser user;

    public List<ModelProductDetail> getProductDetails() {
        return productDetails;
    }

    public void setProductDetails(List<ModelProductDetail> productDetails) {
        this.productDetails = productDetails;
    }

    public ModelProfileSeller getProfilSeller() {
        return profilSeller;
    }

    public void setProfilSeller(ModelProfileSeller profilSeller) {
        this.profilSeller = profilSeller;
    }

    public String getProductTags() {
        return productTags;
    }

    public void setProductTags(String productTags) {
        this.productTags = productTags;
    }

    public ModelReferalUser getUser() {
        return user;
    }

    public void setUser(ModelReferalUser user) {
        this.user = user;
    }
}
