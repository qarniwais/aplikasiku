package com.telkom.aplikasiku.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ilhamsaputra on 12/7/17.
 */

public class ModelFavoriteProduct {
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("sale_price")
    @Expose
    private String salePrice;
    @SerializedName("referal_fee")
    @Expose
    private String referalFee;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(String salePrice) {
        this.salePrice = salePrice;
    }

    public String getReferalFee() {
        return referalFee;
    }

    public void setReferalFee(String referalFee) {
        this.referalFee = referalFee;
    }
}
