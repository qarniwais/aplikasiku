package com.telkom.aplikasiku.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by HP on 12/6/2017.
 */

public class ModelProductDetail {

    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("rating_num")
    @Expose
    private String ratingNum;
    @SerializedName("rating_total")
    @Expose
    private String ratingTotal;
    @SerializedName("rating_user")
    @Expose
    private String ratingUser;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("added_by")
    @Expose
    private String addedBy;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("sub_category")
    @Expose
    private String subCategory;
    @SerializedName("num_of_imgs")
    @Expose
    private String numOfImgs;
    @SerializedName("sale_price")
    @Expose
    private String salePrice;
    @SerializedName("purchase_price")
    @Expose
    private String purchasePrice;
    @SerializedName("shipping_cost")
    @Expose
    private String shippingCost;
    @SerializedName("add_timestamp")
    @Expose
    private String addTimestamp;
    @SerializedName("featured")
    @Expose
    private String featured;
    @SerializedName("tag")
    @Expose
    private String tag;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("front_image")
    @Expose
    private Object frontImage;
    @SerializedName("brand")
    @Expose
    private Object brand;
    @SerializedName("current_stock")
    @Expose
    private String currentStock;
    @SerializedName("unit")
    @Expose
    private Object unit;
    @SerializedName("additional_fields")
    @Expose
    private String additionalFields;
    @SerializedName("number_of_view")
    @Expose
    private String numberOfView;
    @SerializedName("background")
    @Expose
    private Object background;
    @SerializedName("discount")
    @Expose
    private String discount;
    @SerializedName("discount_type")
    @Expose
    private String discountType;
    @SerializedName("tax")
    @Expose
    private String tax;
    @SerializedName("tax_type")
    @Expose
    private String taxType;
    @SerializedName("color")
    @Expose
    private Object color;
    @SerializedName("options")
    @Expose
    private Object options;
    @SerializedName("main_image")
    @Expose
    private String mainImage;
    @SerializedName("download")
    @Expose
    private String download;
    @SerializedName("download_name")
    @Expose
    private String downloadName;
    @SerializedName("deal")
    @Expose
    private Object deal;
    @SerializedName("num_of_downloads")
    @Expose
    private String numOfDownloads;
    @SerializedName("update_time")
    @Expose
    private String updateTime;
    @SerializedName("requirements")
    @Expose
    private String requirements;
    @SerializedName("logo")
    @Expose
    private String logo;
    @SerializedName("video")
    @Expose
    private String video;
    @SerializedName("last_viewed")
    @Expose
    private String lastViewed;
    @SerializedName("num_of_sale")
    @Expose
    private String numOfSale;
    @SerializedName("pks_id")
    @Expose
    private String pksId;
    @SerializedName("tax_setting")
    @Expose
    private String taxSetting;
    @SerializedName("payment_tax")
    @Expose
    private String paymentTax;
    @SerializedName("var_description")
    @Expose
    private Object varDescription;
    @SerializedName("referal_fee")
    @Expose
    private String referalFee;
    @SerializedName("vendor_id")
    @Expose
    private String vendorId;
    @SerializedName("faq")
    @Expose
    private Object faq;
    @SerializedName("expired_period")
    @Expose
    private String expiredPeriod;
    @SerializedName("no_hp")
    @Expose
    private Object noHp;
    @SerializedName("email")
    @Expose
    private Object email;
    @SerializedName("url_web")
    @Expose
    private Object urlWeb;
    @SerializedName("status_approval")
    @Expose
    private String statusApproval;
    @SerializedName("main_photos")
    @Expose
    private String mainPhotos;
    @SerializedName("photos")
    @Expose
    private String photos;
    @SerializedName("url_api")
    @Expose
    private Object urlApi;
    @SerializedName("email_api")
    @Expose
    private Object emailApi;
    @SerializedName("testing_flag")
    @Expose
    private Object testingFlag;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getRatingNum() {
        return ratingNum;
    }

    public void setRatingNum(String ratingNum) {
        this.ratingNum = ratingNum;
    }

    public String getRatingTotal() {
        return ratingTotal;
    }

    public void setRatingTotal(String ratingTotal) {
        this.ratingTotal = ratingTotal;
    }

    public String getRatingUser() {
        return ratingUser;
    }

    public void setRatingUser(String ratingUser) {
        this.ratingUser = ratingUser;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(String addedBy) {
        this.addedBy = addedBy;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public String getNumOfImgs() {
        return numOfImgs;
    }

    public void setNumOfImgs(String numOfImgs) {
        this.numOfImgs = numOfImgs;
    }

    public String getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(String salePrice) {
        this.salePrice = salePrice;
    }

    public String getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(String purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public String getShippingCost() {
        return shippingCost;
    }

    public void setShippingCost(String shippingCost) {
        this.shippingCost = shippingCost;
    }

    public String getAddTimestamp() {
        return addTimestamp;
    }

    public void setAddTimestamp(String addTimestamp) {
        this.addTimestamp = addTimestamp;
    }

    public String getFeatured() {
        return featured;
    }

    public void setFeatured(String featured) {
        this.featured = featured;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getFrontImage() {
        return frontImage;
    }

    public void setFrontImage(Object frontImage) {
        this.frontImage = frontImage;
    }

    public Object getBrand() {
        return brand;
    }

    public void setBrand(Object brand) {
        this.brand = brand;
    }

    public String getCurrentStock() {
        return currentStock;
    }

    public void setCurrentStock(String currentStock) {
        this.currentStock = currentStock;
    }

    public Object getUnit() {
        return unit;
    }

    public void setUnit(Object unit) {
        this.unit = unit;
    }

    public String getAdditionalFields() {
        return additionalFields;
    }

    public void setAdditionalFields(String additionalFields) {
        this.additionalFields = additionalFields;
    }

    public String getNumberOfView() {
        return numberOfView;
    }

    public void setNumberOfView(String numberOfView) {
        this.numberOfView = numberOfView;
    }

    public Object getBackground() {
        return background;
    }

    public void setBackground(Object background) {
        this.background = background;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getTaxType() {
        return taxType;
    }

    public void setTaxType(String taxType) {
        this.taxType = taxType;
    }

    public Object getColor() {
        return color;
    }

    public void setColor(Object color) {
        this.color = color;
    }

    public Object getOptions() {
        return options;
    }

    public void setOptions(Object options) {
        this.options = options;
    }

    public String getMainImage() {
        return mainImage;
    }

    public void setMainImage(String mainImage) {
        this.mainImage = mainImage;
    }

    public String getDownload() {
        return download;
    }

    public void setDownload(String download) {
        this.download = download;
    }

    public String getDownloadName() {
        return downloadName;
    }

    public void setDownloadName(String downloadName) {
        this.downloadName = downloadName;
    }

    public Object getDeal() {
        return deal;
    }

    public void setDeal(Object deal) {
        this.deal = deal;
    }

    public String getNumOfDownloads() {
        return numOfDownloads;
    }

    public void setNumOfDownloads(String numOfDownloads) {
        this.numOfDownloads = numOfDownloads;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getRequirements() {
        return requirements;
    }

    public void setRequirements(String requirements) {
        this.requirements = requirements;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getLastViewed() {
        return lastViewed;
    }

    public void setLastViewed(String lastViewed) {
        this.lastViewed = lastViewed;
    }

    public String getNumOfSale() {
        return numOfSale;
    }

    public void setNumOfSale(String numOfSale) {
        this.numOfSale = numOfSale;
    }

    public String getPksId() {
        return pksId;
    }

    public void setPksId(String pksId) {
        this.pksId = pksId;
    }

    public String getTaxSetting() {
        return taxSetting;
    }

    public void setTaxSetting(String taxSetting) {
        this.taxSetting = taxSetting;
    }

    public String getPaymentTax() {
        return paymentTax;
    }

    public void setPaymentTax(String paymentTax) {
        this.paymentTax = paymentTax;
    }

    public Object getVarDescription() {
        return varDescription;
    }

    public void setVarDescription(Object varDescription) {
        this.varDescription = varDescription;
    }

    public String getReferalFee() {
        return referalFee;
    }

    public void setReferalFee(String referalFee) {
        this.referalFee = referalFee;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public Object getFaq() {
        return faq;
    }

    public void setFaq(Object faq) {
        this.faq = faq;
    }

    public String getExpiredPeriod() {
        return expiredPeriod;
    }

    public void setExpiredPeriod(String expiredPeriod) {
        this.expiredPeriod = expiredPeriod;
    }

    public Object getNoHp() {
        return noHp;
    }

    public void setNoHp(Object noHp) {
        this.noHp = noHp;
    }

    public Object getEmail() {
        return email;
    }

    public void setEmail(Object email) {
        this.email = email;
    }

    public Object getUrlWeb() {
        return urlWeb;
    }

    public void setUrlWeb(Object urlWeb) {
        this.urlWeb = urlWeb;
    }

    public String getStatusApproval() {
        return statusApproval;
    }

    public void setStatusApproval(String statusApproval) {
        this.statusApproval = statusApproval;
    }

    public String getMainPhotos() {
        return mainPhotos;
    }

    public void setMainPhotos(String mainPhotos) {
        this.mainPhotos = mainPhotos;
    }

    public String getPhotos() {
        return photos;
    }

    public void setPhotos(String photos) {
        this.photos = photos;
    }

    public Object getUrlApi() {
        return urlApi;
    }

    public void setUrlApi(Object urlApi) {
        this.urlApi = urlApi;
    }

    public Object getEmailApi() {
        return emailApi;
    }

    public void setEmailApi(Object emailApi) {
        this.emailApi = emailApi;
    }

    public Object getTestingFlag() {
        return testingFlag;
    }

    public void setTestingFlag(Object testingFlag) {
        this.testingFlag = testingFlag;
    }


}
