package com.telkom.aplikasiku.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by HP on 12/6/2017.
 */

public class ModelProfile {

    @SerializedName("profil_seller")
    @Expose
    private ModelProfileSeller profilSeller;

    public ModelProfileSeller getProfilSeller() {
        return profilSeller;
    }

    public void setProfilSeller(ModelProfileSeller profilSeller) {
        this.profilSeller = profilSeller;
    }
}
