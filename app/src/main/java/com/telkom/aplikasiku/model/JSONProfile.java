package com.telkom.aplikasiku.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by HP on 12/6/2017.
 */

public class JSONProfile {
        @SerializedName("seller_id")
        @Expose
        private String sellerId;
        @SerializedName("token")
        @Expose
        private String token;
        @SerializedName("os_version")
        @Expose
        private String osVersion;
        @SerializedName("hp_type")
        @Expose
        private String hpType;

        public String getSellerId() {
            return sellerId;
        }

        public void setSellerId(String sellerId) {
            this.sellerId = sellerId;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getOsVersion() {
            return osVersion;
        }

        public void setOsVersion(String osVersion) {
            this.osVersion = osVersion;
        }

        public String getHpType() {
            return hpType;
        }

        public void setHpType(String hpType) {
            this.hpType = hpType;
        }
}
