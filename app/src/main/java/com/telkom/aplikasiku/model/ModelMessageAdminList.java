package com.telkom.aplikasiku.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ilhamsaputra on 12/7/17.
 */

public class ModelMessageAdminList {
    @SerializedName("ticket_id")
    @Expose
    private String ticketId;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("from_where")
    @Expose
    private String fromWhere;
    @SerializedName("to_where")
    @Expose
    private String toWhere;
    @SerializedName("subject")
    @Expose
    private String subject;
    @SerializedName("view_status")
    @Expose
    private String viewStatus;
    @SerializedName("close")
    @Expose
    private String close;

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getFromWhere() {
        return fromWhere;
    }

    public void setFromWhere(String fromWhere) {
        this.fromWhere = fromWhere;
    }

    public String getToWhere() {
        return toWhere;
    }

    public void setToWhere(String toWhere) {
        this.toWhere = toWhere;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getViewStatus() {
        return viewStatus;
    }

    public void setViewStatus(String viewStatus) {
        this.viewStatus = viewStatus;
    }

    public String getClose() {
        return close;
    }

    public void setClose(String close) {
        this.close = close;
    }
}
