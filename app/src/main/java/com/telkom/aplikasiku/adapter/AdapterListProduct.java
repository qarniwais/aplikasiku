package com.telkom.aplikasiku.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.telkom.aplikasiku.R;
import com.telkom.aplikasiku.main.detailproduk.DetailProdukActivity;
import com.telkom.aplikasiku.model.ModelListProduk;

import java.util.List;

/**
 * Created by HP on 12/4/2017.
 */

public class AdapterListProduct extends RecyclerView.Adapter<AdapterListProduct.HolderData> {

    private List<ModelListProduk> list;
    private Context context;

    public AdapterListProduct(Context context, List<ModelListProduk> list) {
        this.context = context;
        this.list = list;

    }

    @Override
    public HolderData onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.produk_grid, parent, false);
        HolderData holderData = new HolderData(view);
        return holderData;
    }

    @Override
    public void onBindViewHolder(HolderData holder, int position) {
        ModelListProduk modelListProduk = list.get(position);
        holder.tv_title.setText(modelListProduk.getTitle());
        holder.tv_id.setText(modelListProduk.getProduct_id());
        holder.tv_desk.setText(modelListProduk.getDescription());
        holder.tv_harga.setText(modelListProduk.getReferal_fee());
        holder.tv_rating.setRating(Float.parseFloat(modelListProduk.getRating_total()));
        Picasso.with(context)
                .load(modelListProduk.getMain_photos())
                .placeholder(R.drawable.placeholder)
                .into(holder.imageView);
        holder.listProduk = modelListProduk;


    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    class HolderData extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView tv_id;
        TextView tv_desk;
        TextView tv_title;
        TextView tv_harga;
        RatingBar tv_rating;
        ModelListProduk listProduk;


        public HolderData(View itemView) {
            super(itemView);

            imageView = (ImageView)itemView.findViewById(R.id.iv_image_produk);
            tv_id = (TextView)itemView.findViewById(R.id.tv_id);
            tv_desk = (TextView)itemView.findViewById(R.id.tv_deskripsi);
            tv_title = (TextView)itemView.findViewById(R.id.tv_title);
            tv_harga = (TextView)itemView.findViewById(R.id.tv_harga);
            tv_rating = (RatingBar)itemView.findViewById(R.id.ratingBar1);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(v.getContext(), DetailProdukActivity.class);
                    intent.putExtra("product_id", listProduk.getProduct_id());
                    intent.putExtra("title", listProduk.getTitle());
                    intent.putExtra("harga", listProduk.getReferal_fee());
                    intent.putExtra("rating", listProduk.getRating_total());
                    intent.putExtra("deskripsi", listProduk.getDescription());
                    intent.putExtra("image", listProduk.getMain_photos());
                    v.getContext().startActivity(intent);

                }
            });
        }
    }
}
