package com.telkom.aplikasiku.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by tompihariadi on 07/12/17.
 */

public class RequestPaymentResponse {

    @SerializedName("invoice")
    @Expose
    private String invoice;
    @SerializedName("paycode")
    @Expose
    private String paycode;
    @SerializedName("status")
    @Expose
    private String status;

    public String getInvoice() {
        return invoice;
    }

    public void setInvoice(String invoice) {
        this.invoice = invoice;
    }

    public String getPaycode() {
        return paycode;
    }

    public void setPaycode(String paycode) {
        this.paycode = paycode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}