package com.telkom.aplikasiku.responses;

import com.google.gson.annotations.SerializedName;
import com.telkom.aplikasiku.model.ModelLogin;

/**
 * Created by HP on 12/6/2017.
 */

public class LoginResponse {

    @SerializedName("data")
    private ModelLogin login;

    public ModelLogin getLogin() {
        return login;
    }

    public void setLogin(ModelLogin login) {
        this.login = login;
    }
}
