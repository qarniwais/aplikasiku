package com.telkom.aplikasiku.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.telkom.aplikasiku.model.ModelMessageAdminList;

import java.util.List;

/**
 * Created by ilhamsaputra on 12/7/17.
 */

public class ResponMessageAdminList {
    @SerializedName("data")
    @Expose
    private List<ModelMessageAdminList> data = null;

    public List<ModelMessageAdminList> getData() {
        return data;
    }

    public void setData(List<ModelMessageAdminList> data) {
        this.data = data;
    }
}
