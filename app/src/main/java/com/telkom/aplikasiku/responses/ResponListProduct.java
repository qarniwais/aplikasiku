package com.telkom.aplikasiku.responses;

import com.telkom.aplikasiku.model.ModelListProduk;

import java.util.List;

/**
 * Created by HP on 12/4/2017.
 */

public class ResponListProduct {

    List<ModelListProduk> data;

    public List<ModelListProduk> getData() {
        return data;
    }

    public void setData(List<ModelListProduk> data) {
        this.data = data;
    }
}
