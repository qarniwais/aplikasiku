package com.telkom.aplikasiku.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.telkom.aplikasiku.model.ModelDataVendorDetail;
import com.telkom.aplikasiku.model.ModelVendorDetail;

/**
 * Created by ilhamsaputra on 12/7/17.
 */

public class ResponVendorDetail {
    @SerializedName("data")
    @Expose
    private ModelVendorDetail data;

    public ModelVendorDetail getData() {
        return data;
    }

    public void setData(ModelVendorDetail data) {
        this.data = data;
    }
}
