package com.telkom.aplikasiku.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.telkom.aplikasiku.model.Payment;

/**
 * Created by tompihariadi on 07/12/17.
 */

public class PaymentResponse {

    @SerializedName("ATM")
    @Expose
    private Payment aTM;
    @SerializedName("Ibank")
    @Expose
    private Payment ibank;

    public Payment getATM() {
        return aTM;
    }

    public void setATM(Payment aTM) {
        this.aTM = aTM;
    }

    public Payment getIbank() {
        return ibank;
    }

    public void setIbank(Payment ibank) {
        this.ibank = ibank;
    }

}