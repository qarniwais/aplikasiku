package com.telkom.aplikasiku.responses;

import com.google.gson.annotations.Expose;

/**
 * Created by ilhamsaputra on 12/7/17.
 */

public class ResponLogoutVendor {
    @Expose
    private String data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
