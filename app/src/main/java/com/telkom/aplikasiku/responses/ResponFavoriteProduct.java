package com.telkom.aplikasiku.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.telkom.aplikasiku.model.ModelDataFavoriteProduct;

/**
 * Created by ilhamsaputra on 12/7/17.
 */

public class ResponFavoriteProduct {
    @SerializedName("data")
    @Expose
    private ModelDataFavoriteProduct data;

    public ModelDataFavoriteProduct getData() {
        return data;
    }

    public void setData(ModelDataFavoriteProduct data) {
        this.data = data;
    }
}
