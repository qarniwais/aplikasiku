package com.telkom.aplikasiku.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.telkom.aplikasiku.model.ModelVendorReportTmoneySaldo;

import java.util.List;

/**
 * Created by ilhamsaputra on 12/7/17.
 */

public class ResponVendorReportTmoneySaldo {
    @SerializedName("data")
    @Expose
    private List<ModelVendorReportTmoneySaldo> data = null;

    public List<ModelVendorReportTmoneySaldo> getData() {
        return data;
    }

    public void setData(List<ModelVendorReportTmoneySaldo> data) {
        this.data = data;
    }
}
