package com.telkom.aplikasiku.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.telkom.aplikasiku.model.ModelProfile;

/**
 * Created by HP on 12/6/2017.
 */

public class ResponProfile {

    @SerializedName("data")
    @Expose
    private ModelProfile data;

    public ModelProfile getData() {
        return data;
    }

    public void setData(ModelProfile data) {
        this.data = data;
    }
}
