package com.telkom.aplikasiku.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.telkom.aplikasiku.model.ModelEditVendorInfo;

import java.util.List;

/**
 * Created by ilhamsaputra on 12/7/17.
 */

public class ResponEditVendorInfo {
    @SerializedName("data")
    @Expose
    private List<ModelEditVendorInfo> data = null;

    public List<ModelEditVendorInfo> getData() {
        return data;
    }

    public void setData(List<ModelEditVendorInfo> data) {
        this.data = data;
    }
}
