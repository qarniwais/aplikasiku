package com.telkom.aplikasiku.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.telkom.aplikasiku.model.ModelDataProductDetail;

/**
 * Created by trisa on 08/12/2017.
 */

public class ResponseProductProfile {
    @SerializedName("data")
    @Expose
    private ModelDataProductDetail data;

    public ModelDataProductDetail getData() {
        return data;
    }

    public void setData(ModelDataProductDetail data) {
        this.data = data;
    }
}
