package com.telkom.aplikasiku.utils;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by ilhamsaputra on 12/13/16.
 */
public class AplikasikuProgressDialog {
    static ProgressDialog dialog;

    public static void show(Context context){
        dialog = new ProgressDialog(context);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        if (!dialog.isShowing()){
            dialog.show();
        }
    }

    public static void dismiss(){
        dialog.dismiss();
    }
}
