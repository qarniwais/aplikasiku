package com.telkom.aplikasiku.utils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;


public class Utility {

    public static final String CODERESPONDEN = "R";
    public static final String CODEASNWER = "A";
    public static final String CODEQUESTION = "Q";
    public static final String CODESURVEYOR = "S";

    public static String today(){
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();

        String d = format.format(date).toString();
        return d;
    }

    private static String getTime(){
        Calendar cal = Calendar.getInstance(TimeZone.getDefault());
        Date currentLocalTime = cal.getTime();
        SimpleDateFormat date = new SimpleDateFormat("HH:mm:ss");
        //you can get seconds by adding  "...:ss" to it
        date.setTimeZone(TimeZone.getDefault());
        String localTime = date.format(currentLocalTime);

        return localTime;
    }


    public static String gettanggal(String unix){
        int unit = Integer.parseInt(unix);
        long dv = (long) unit *1000;// its need to be in milisecond
        Date df = new java.util.Date(dv);
        String vv = new SimpleDateFormat("yyyy-MM-dd").format(df);
        return vv;
    }

    public static String getbulan(){
        String a = today();
        String[] a1 = a.split("-");
        String dd = a1[0];
        String mm = a1[1];
        String yy = a1[2];
        int bulan = Integer.parseInt(mm);
        return String.valueOf(bulan);
    }
    public static String gettahun(){
        String a = today();
        String[] a1 = a.split("-");
        String dd = a1[0];
        String mm = a1[1];
        String yy = a1[2];
        return yy;
    }

    public static String todayWithTime(){
        return today()+" "+getTime();
    }

    public static String generateCode(String kode){
        String a = today();
        String b = getTime();

        String[] a1 = a.split("-");
        String[] b1 = b.split(":");

        String dd = a1[0];
        String mm = a1[1];
        String yy = a1[2];

        String h = b1[0];
        String m = b1[1];
        String s = b1[2];

        int min = 10;
        int max = 99;

        Random r = new Random();
        int i1 = r.nextInt(max - min + 1) + min;

        String result = s+yy+m+mm+h+dd+i1;

        return result;
    }


    private static String hasil;

    public static String convertRP(String nilai){
        if (nilai==null||nilai.matches("")){
            nilai = "0";
        }
        NumberFormat df = NumberFormat.getCurrencyInstance();
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setCurrencySymbol("Rp ");
        dfs.setGroupingSeparator('.');
        dfs.setMonetaryDecimalSeparator('.');
        df.setMaximumFractionDigits(0);
        ((DecimalFormat) df).setDecimalFormatSymbols(dfs);

        hasil = df.format(Integer.parseInt(nilai));

        return hasil;
    }

}
