package com.telkom.aplikasiku.utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.telkom.aplikasiku.main.halamanutama.FragmentHalamanUtama;
import com.telkom.aplikasiku.main.prajoinaplikasiku.PraJoinActivity;

import java.util.HashMap;

/**
 * Created by HP on 12/5/2017.
 */

public class SessionManager {

    SharedPreferences preferences;

    SharedPreferences.Editor editor;

    Context context;

    int PRIVATE_MODE = 0;

    public static final String PREF_NAME = "Aplikasiku";
    public static final String IS_LOGIN = "IsLoggedIn";

    public static final String KEY_NAME = "name";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_USER_ID = "userId";
    public static final String KEY_PHONE_PEMBELI = "phone_pembeli";
    public static final String KEY_NAME_PEMBELI = "name_pembeli";
    public static final String KEY_EMAIL_PEMBELI = "email_pembeli";

    public SessionManager(Context context){
        this.context = context;
        preferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = preferences.edit();
    }

    public void createLoginSession(String name, String email, String id){
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(KEY_NAME, name);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_USER_ID, id);
        editor.commit();
    }

    public void dataPembeli(String name_pembeli, String email_pembeli, String phone_pembeli){
        editor.putString(KEY_NAME_PEMBELI, name_pembeli);
        editor.putString(KEY_EMAIL_PEMBELI, email_pembeli);
        editor.putString(KEY_PHONE_PEMBELI, phone_pembeli);
        editor.commit();
    }

    public void checkLogin(){
        if (!this.isLoggedIn()){
            Intent intent = new Intent(context, FragmentHalamanUtama.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            context.startActivity(intent);
        }
    }

    public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<String, String>();

        //user
        user.put(KEY_NAME, preferences.getString(KEY_NAME, null));
        user.put(KEY_EMAIL, preferences.getString(KEY_EMAIL, null));
        user.put(KEY_USER_ID, preferences.getString(KEY_USER_ID, ""));

        return user;
    }

    public HashMap<String, String> getDataPembeli(){
        HashMap<String, String> user = new HashMap<String, String>();

        //user
        user.put(KEY_NAME_PEMBELI, preferences.getString(KEY_NAME_PEMBELI, ""));
        user.put(KEY_EMAIL_PEMBELI, preferences.getString(KEY_EMAIL_PEMBELI, ""));
        user.put(KEY_PHONE_PEMBELI, preferences.getString(KEY_PHONE_PEMBELI, ""));

        return user;
    }

    public void logoutUser(){
        editor.clear();
        editor.commit();

        Intent intent = new Intent(context, PraJoinActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        context.startActivity(intent);


    }

    public String  getUserId(){
        return preferences.getString(KEY_USER_ID, "");
    }


    public boolean isLoggedIn() {
        return preferences.getBoolean(IS_LOGIN, false);
    }

}
