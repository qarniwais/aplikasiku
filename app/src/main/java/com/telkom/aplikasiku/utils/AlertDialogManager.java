package com.telkom.aplikasiku.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.telkom.aplikasiku.R;

/**
 * Created by HP on 12/4/2017.
 */

public class AlertDialogManager {

    public void showAlertDialog(Context context, String title, String message, Boolean status){
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        alertDialog.setTitle(title);

        alertDialog.setMessage(message);

        if (status != null)
            alertDialog.setIcon((status) ? R.drawable.success : R.drawable.failed);

        alertDialog.setButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alertDialog.show();
    }
}
