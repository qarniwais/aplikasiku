package com.telkom.aplikasiku.deserialization;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.telkom.aplikasiku.model.ModelProfile;
import com.telkom.aplikasiku.responses.ResponProfile;

import java.lang.reflect.Type;

import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by HP on 12/6/2017.
 */

public class ProfileDeserializer implements JsonDeserializer<ResponProfile> {

    @Override
    public ResponProfile deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Gson gson = new Gson();
        ResponProfile response = gson.fromJson(json, ResponProfile.class);

        JsonObject jsonObject = json.getAsJsonObject();
        JsonElement element = jsonObject.get("data");

        ModelProfile user = gson.fromJson(element, ModelProfile.class);
        response.setData(user);
        return response;
    }

    public static GsonConverterFactory converterFactory() {
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss")
                .registerTypeAdapter(ResponProfile.class, new ProfileDeserializer())
                .setLenient()
                .create();

        return GsonConverterFactory.create(gson);
    }
}
