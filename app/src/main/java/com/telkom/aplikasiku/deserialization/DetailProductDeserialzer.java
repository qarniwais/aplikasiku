package com.telkom.aplikasiku.deserialization;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.telkom.aplikasiku.responses.ResponseProductProfile;

import org.json.JSONObject;

import java.lang.reflect.Type;

import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by trisa on 08/12/2017.
 */

public class DetailProductDeserialzer implements JsonDeserializer<ResponseProductProfile> {
    @Override
    public ResponseProductProfile deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Gson gson = new Gson();
        ResponseProductProfile response = gson.fromJson(json, ResponseProductProfile.class);

//        JsonObject object = json.getAsJsonObject();
//        JsonElement element = object.getAsJsonObject("data");

        //response.

        return response;
    }

    public static GsonConverterFactory converterFactory() {
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss")
                .registerTypeAdapter(ResponseProductProfile.class, new DetailProductDeserialzer())
                .setLenient()
                .create();

        return GsonConverterFactory.create(gson);
    }
}
