package com.telkom.aplikasiku.deserialization;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.telkom.aplikasiku.model.ModelDataVendorDetail;
import com.telkom.aplikasiku.model.ModelVendorDetail;
import com.telkom.aplikasiku.responses.ResponVendorDetail;

import java.lang.reflect.Type;

import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ilhamsaputra on 12/7/17.
 */

public class VendorDetailDeserializer implements JsonDeserializer<ResponVendorDetail> {

    @Override
    public ResponVendorDetail deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Gson gson = new Gson();
        ResponVendorDetail response = gson.fromJson(json, ResponVendorDetail.class);

        JsonObject jsonObject = json.getAsJsonObject();
        System.out.println("array = "+jsonObject.toString());
        JsonArray arr =  jsonObject.getAsJsonObject("data").getAsJsonArray("vendor_detail");
        System.out.println("array = "+arr.toString());
        JsonElement element = arr.get(0).getAsJsonObject();

        ModelVendorDetail vendorDetail = gson.fromJson(element, ModelVendorDetail.class);
        response.setData(vendorDetail);

        return response;
    }

    public static GsonConverterFactory converterFactory() {
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss")
                .registerTypeAdapter(ResponVendorDetail.class, new VendorDetailDeserializer())
                .setLenient()
                .create();

        return GsonConverterFactory.create(gson);
    }
}
