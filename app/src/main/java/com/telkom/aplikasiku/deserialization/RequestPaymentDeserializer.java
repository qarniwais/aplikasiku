package com.telkom.aplikasiku.deserialization;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.telkom.aplikasiku.responses.RequestPaymentResponse;

import java.lang.reflect.Type;

import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by tompihariadi on 07/12/17.
 */

public class RequestPaymentDeserializer implements JsonDeserializer<RequestPaymentResponse> {

    @Override
    public RequestPaymentResponse deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Gson gson = new Gson();
        RequestPaymentResponse response = gson.fromJson(json, RequestPaymentResponse.class);

        return response;
    }

    public static GsonConverterFactory converterFactory() {
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss")
                .registerTypeAdapter(RequestPaymentResponse.class, new RequestPaymentDeserializer())
                .setLenient()
                .create();

        return GsonConverterFactory.create(gson);
    }
}
