package com.telkom.aplikasiku.deserialization;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import com.telkom.aplikasiku.model.ModelMessageAdminList;
import com.telkom.aplikasiku.responses.ResponMessageAdminList;

import java.lang.reflect.Type;
import java.util.List;

import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ilhamsaputra on 12/7/17.
 */

public class MessageAdminListDeserializer implements JsonDeserializer<ResponMessageAdminList> {

    @Override
    public ResponMessageAdminList deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Gson gson = new Gson();
        ResponMessageAdminList response = gson.fromJson(json, ResponMessageAdminList.class);

        JsonObject jsonObject = json.getAsJsonObject();
        JsonArray arr = jsonObject.getAsJsonArray("data");

        Type listType = new TypeToken<List<ModelMessageAdminList>>() {
        }.getType();
        List<ModelMessageAdminList> orders = gson.fromJson(arr, listType);

        response.setData(orders);

        return response;
    }

    public static GsonConverterFactory converterFactory() {
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss")
                .registerTypeAdapter(ResponMessageAdminList.class, new MessageAdminListDeserializer())
                .setLenient()
                .create();

        return GsonConverterFactory.create(gson);
    }
}