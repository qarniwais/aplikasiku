package com.telkom.aplikasiku.deserialization;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.telkom.aplikasiku.model.ModelDataFavoriteProduct;
import com.telkom.aplikasiku.responses.ResponFavoriteProduct;

import java.lang.reflect.Type;

import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ilhamsaputra on 12/7/17.
 */

public class FavoriteProductDeserializer implements JsonDeserializer<ResponFavoriteProduct> {

    @Override
    public ResponFavoriteProduct deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Gson gson = new Gson();
        ResponFavoriteProduct response = gson.fromJson(json, ResponFavoriteProduct.class);

        JsonObject jsonObject = json.getAsJsonObject();
        JsonElement element = jsonObject.getAsJsonObject("data");

        ModelDataFavoriteProduct favoriteProduct = gson.fromJson(element, ModelDataFavoriteProduct.class);
        response.setData(favoriteProduct);

        return response;
    }

    public static GsonConverterFactory converterFactory() {
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss")
                .registerTypeAdapter(ResponFavoriteProduct.class, new FavoriteProductDeserializer())
                .setLenient()
                .create();

        return GsonConverterFactory.create(gson);
    }
}
