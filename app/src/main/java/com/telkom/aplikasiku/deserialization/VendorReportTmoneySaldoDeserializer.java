package com.telkom.aplikasiku.deserialization;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import com.telkom.aplikasiku.model.ModelVendorReportTmoneySaldo;
import com.telkom.aplikasiku.responses.ResponVendorReportTmoneySaldo;

import java.lang.reflect.Type;
import java.util.List;

import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ilhamsaputra on 12/7/17.
 */

public class VendorReportTmoneySaldoDeserializer implements JsonDeserializer<ResponVendorReportTmoneySaldo> {

    @Override
    public ResponVendorReportTmoneySaldo deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Gson gson = new Gson();
        ResponVendorReportTmoneySaldo response = gson.fromJson(json, ResponVendorReportTmoneySaldo.class);

        JsonObject jsonObject = json.getAsJsonObject();
        JsonArray arr = jsonObject.getAsJsonArray("data");

        Type listType = new TypeToken<List<ModelVendorReportTmoneySaldo>>() {
        }.getType();
        List<ModelVendorReportTmoneySaldo> orders = gson.fromJson(arr, listType);

        response.setData(orders);

        return response;
    }

    public static GsonConverterFactory converterFactory() {
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss")
                .registerTypeAdapter(ResponVendorReportTmoneySaldo.class, new VendorReportTmoneySaldoDeserializer())
                .setLenient()
                .create();

        return GsonConverterFactory.create(gson);
    }
}