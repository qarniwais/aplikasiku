package com.telkom.aplikasiku.deserialization;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.telkom.aplikasiku.model.Payment;
import com.telkom.aplikasiku.responses.PaymentResponse;

import java.lang.reflect.Type;

import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by tompihariadi on 07/12/17.
 */

public class PaymentDeserializer implements JsonDeserializer<PaymentResponse> {

    @Override
    public PaymentResponse deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Gson gson = new Gson();
        PaymentResponse response = gson.fromJson(json, PaymentResponse.class);

        JsonObject jsonObject = json.getAsJsonObject();
        JsonElement elementAtm = jsonObject.getAsJsonObject("Finpay").get("ATM");
        Payment atm = gson.fromJson(elementAtm, Payment.class);
        response.setATM(atm);

        JsonElement elementIbank = jsonObject.getAsJsonObject("Finpay").get("Ibank");
        Payment ibank = gson.fromJson(elementIbank, Payment.class);
        response.setIbank(ibank);

        return response;
    }

    public static GsonConverterFactory converterFactory() {
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss")
                .registerTypeAdapter(PaymentResponse.class, new PaymentDeserializer())
                .setLenient()
                .create();

        return GsonConverterFactory.create(gson);
    }
}
