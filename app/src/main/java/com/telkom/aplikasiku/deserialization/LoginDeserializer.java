package com.telkom.aplikasiku.deserialization;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.telkom.aplikasiku.model.ModelLogin;
import com.telkom.aplikasiku.responses.LoginResponse;

import java.lang.reflect.Type;

import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by HP on 12/6/2017.
 */

public class LoginDeserializer  implements JsonDeserializer<LoginResponse> {

    @Override
    public LoginResponse deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Gson gson = new Gson();
        LoginResponse response = gson.fromJson(json, LoginResponse.class);

        JsonObject jsonObject = json.getAsJsonObject();
        JsonElement element = jsonObject.get("data");

        ModelLogin user = gson.fromJson(element, ModelLogin.class);
        response.setLogin(user);
        return response;
    }

    public static GsonConverterFactory converterFactory(){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss")
                .registerTypeAdapter(LoginResponse.class, new LoginDeserializer())
                .setLenient()
                .create();

        return GsonConverterFactory.create(gson);
    }
}