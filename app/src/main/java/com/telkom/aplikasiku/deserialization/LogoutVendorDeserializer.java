package com.telkom.aplikasiku.deserialization;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.telkom.aplikasiku.responses.ResponLogoutVendor;

import java.lang.reflect.Type;

import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ilhamsaputra on 12/7/17.
 */

public class LogoutVendorDeserializer implements JsonDeserializer<ResponLogoutVendor> {

    @Override
    public ResponLogoutVendor deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Gson gson = new Gson();
        ResponLogoutVendor response = gson.fromJson(json, ResponLogoutVendor.class);

        JsonObject jsonObject = json.getAsJsonObject();
        JsonElement element = jsonObject.get("data");

        String user = gson.fromJson(element,String.class);
        response.setData(user);

        return response;
    }

    public static GsonConverterFactory converterFactory(){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss")
                .registerTypeAdapter(ResponLogoutVendor.class, new LogoutVendorDeserializer())
                .setLenient()
                .create();

        return GsonConverterFactory.create(gson);
    }
}