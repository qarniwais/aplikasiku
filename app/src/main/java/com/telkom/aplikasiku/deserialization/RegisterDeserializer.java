package com.telkom.aplikasiku.deserialization;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.telkom.aplikasiku.model.ModelRegister;
import com.telkom.aplikasiku.responses.ResponRegister;

import java.lang.reflect.Type;

import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by HP on 12/6/2017.
 */

public class RegisterDeserializer implements JsonDeserializer<ResponRegister> {

@Override
public ResponRegister deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Gson gson = new Gson();
        ResponRegister response = gson.fromJson(json, ResponRegister.class);

        JsonObject jsonObject = json.getAsJsonObject();
        JsonElement element = jsonObject.get("data");

        ModelRegister user = gson.fromJson(element, ModelRegister.class);
        response.setMeta(user);
        return response;
        }

public static GsonConverterFactory converterFactory(){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss")
        .registerTypeAdapter(ResponRegister.class, new RegisterDeserializer())
        .setLenient()
        .create();

        return GsonConverterFactory.create(gson);
        }
}