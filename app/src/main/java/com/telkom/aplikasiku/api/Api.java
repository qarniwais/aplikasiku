package com.telkom.aplikasiku.api;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by tompihariadi on 16-May-17.
 */

public class Api {
    private static final String BASE_URL = "https://prodapi.aplikasiku.id/";
    private static final String BASE_URL_SETTLEMENT = "https://settlement.aplikasiku.id/";

    private static Api instance = new Api();

    public static Api getInstance(){
        return instance;
    }

    public Retrofit getRetrofit(GsonConverterFactory gcf){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss")
                .setLenient()
                .create();

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(gcf)
                .client(httpClient.build())
                .build();

        return retrofit;
    }

    public <S> S createService(Class<S> serviceClass, GsonConverterFactory gcf){
        return getRetrofit(gcf)
                .create(serviceClass);
    }

    public Retrofit getRetrofitGeneral(GsonConverterFactory gcf){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss")
                .setLenient()
                .create();

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL_SETTLEMENT)
                .addConverterFactory(gcf)
                .client(httpClient.build())
                .build();
        return retrofit;
    }

    public <S> S createServiceGeneral(Class<S> serviceClass, GsonConverterFactory gcf){
        return getRetrofitGeneral(gcf)
                .create(serviceClass);
    }
}
