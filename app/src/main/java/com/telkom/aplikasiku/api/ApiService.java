package com.telkom.aplikasiku.api;

import com.telkom.aplikasiku.model.JSONDetailProduct;
import com.telkom.aplikasiku.model.JSONEditVendorInfo;
import com.telkom.aplikasiku.model.JSONFavoriteProduct;
import com.telkom.aplikasiku.model.JSONLogin;
import com.telkom.aplikasiku.model.JSONLogoutVendor;
import com.telkom.aplikasiku.model.JSONMessageAdmin;
import com.telkom.aplikasiku.model.JSONMessageAdminList;
import com.telkom.aplikasiku.model.JSONPaymentMethod;
import com.telkom.aplikasiku.model.JSONProfile;
import com.telkom.aplikasiku.model.JSONRegister;
import com.telkom.aplikasiku.model.JSONRequestPayment;
import com.telkom.aplikasiku.model.JSONVendorDetail;
import com.telkom.aplikasiku.model.JSONVendorReportTmoneySaldo;
import com.telkom.aplikasiku.responses.LoginResponse;
import com.telkom.aplikasiku.responses.PaymentResponse;
import com.telkom.aplikasiku.responses.RequestPaymentResponse;
import com.telkom.aplikasiku.responses.ResponEditVendorInfo;
import com.telkom.aplikasiku.responses.ResponFavoriteProduct;
import com.telkom.aplikasiku.responses.ResponListProduct;
import com.telkom.aplikasiku.responses.ResponLogoutVendor;
import com.telkom.aplikasiku.responses.ResponMessageAdmin;
import com.telkom.aplikasiku.responses.ResponMessageAdminList;
import com.telkom.aplikasiku.responses.ResponPaymenMethods;
import com.telkom.aplikasiku.responses.ResponProfile;
import com.telkom.aplikasiku.responses.ResponRegister;
import com.telkom.aplikasiku.responses.ResponVendorDetail;
import com.telkom.aplikasiku.responses.ResponVendorReportTmoneySaldo;
import com.telkom.aplikasiku.responses.ResponseProductProfile;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by trisa on 24/11/2017.
 */

public interface ApiService {

    @POST("api/reseller/register")
    Call<ResponRegister> sendRegister(@Body JSONRegister jsonRegister);

    @POST("api/reseller/login")
    Call<LoginResponse> login(@Body JSONLogin jsonLogin);

    @POST("api/reseller/product_list")
    Call<ResponListProduct> getData();

    @POST("api/reseller/profile_seller")
    Call<ResponProfile> getUser(@Body JSONProfile jsonProfile);

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    @POST("api/reseller/vendor_detail")
    Call<ResponVendorDetail> getVendorDetail(@Body JSONVendorDetail jsonVendorDetail);

    @POST("api/reseller/favorite_product")
    Call<ResponFavoriteProduct> getFavoriteProduct(@Body JSONFavoriteProduct jsonFavoriteProduct);

    // tidak ada data, tidak bisa melihat bentuk response structure
//    @POST("api/reseller/vendor_report")
//    Call<ResponVendorReport> getVendorReport(@Body JSONVendorReport jsonVendorReport);

    //api error
//    @POST("api/reseller/vendor_report_share")
//    Call<ResponVendorReportShare> getVendorReportShare(@Body JSONVendorReportShare jsonVendorReportShare);

    @POST("api/reseller/vendor_report_tmoney_saldo")
    Call<ResponVendorReportTmoneySaldo> getVendorReportTmoneySaldo(@Body JSONVendorReportTmoneySaldo jsonVendorReportTmoneySaldo);

    @POST("api/reseller/edit_vendor_info")
    Call<ResponEditVendorInfo> getEditVendorInfo(@Body JSONEditVendorInfo jsonEditVendorInfo);

    //tidak ada balikan respon dari api
//    @POST("api/reseller/change_vendor_password")
//    Call<ResponChangeVendorPassword> getChangeVendorPassword(@Body JSONChangeVendorPassword jsonChangeVendorPassword);

    @POST("api/reseller/message_admin_list")
    Call<ResponMessageAdminList> getMessageAdminList(@Body JSONMessageAdminList jsonMessageAdminList);

    @POST("api/reseller/message_admin")
    Call<ResponMessageAdmin> getMessageAdmin(@Body JSONMessageAdmin jsonMessageAdmin);

    @POST("api/reseller/logout_vendor")
    Call<ResponLogoutVendor> getLogoutVendor(@Body JSONLogoutVendor jsonLogoutVendor);

    @POST("api/reseller/payment_methods")
    Call<ResponPaymenMethods> getPaymentMethods(@Body JSONPaymentMethod jsonPaymentMethod);

    @GET("GetMethod")
    Call<PaymentResponse> getPayment();

    @POST("RequestPayment/")
    Call<RequestPaymentResponse> requestPayment(@Body JSONRequestPayment jsonRequestPayment);

    @POST("api/reseller/product_detail")
    Call<ResponseProductProfile> getDetailProduct(@Body JSONDetailProduct param);
}
