package com.telkom.aplikasiku.data.component;

import com.telkom.aplikasiku.data.module.AppModule;
import com.telkom.aplikasiku.data.module.NetModule;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;

/**
 * Created by trisa on 24/11/2017.
 */

@Singleton
@Component(modules = {AppModule.class, NetModule.class})
public interface NetComponent {
    // downstream components need these exposed with the return type
    // method name does not really matter
    Retrofit retrofit();
}
