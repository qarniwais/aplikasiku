package com.telkom.aplikasiku.main.updateprofile;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.telkom.aplikasiku.R;
import com.telkom.aplikasiku.api.Api;
import com.telkom.aplikasiku.api.ApiService;
import com.telkom.aplikasiku.deserialization.EditVendorInfoDeserializer;
import com.telkom.aplikasiku.model.JSONEditVendorInfo;
import com.telkom.aplikasiku.responses.ResponEditVendorInfo;
import com.telkom.aplikasiku.utils.AplikasikuProgressDialog;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateProfileActivity extends AppCompatActivity {

    Button btn_cancel_profile;
    Button btn_update_profile;

    EditText et_userid_profile;
    EditText et_nama_profile;
    EditText et_email_profile;
    EditText et_phone_profile;
    EditText et_token_profile;
    EditText et_alamat_profile;
    EditText et_kota_profile;
    EditText et_provinsi_profile;
    EditText et_negara_profile;

    ProgressDialog dialog;

    String idVendor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);

        btn_cancel_profile = (Button)findViewById(R.id.btn_cancel_profile);
        btn_update_profile = (Button)findViewById(R.id.btn_update_profile);
        et_userid_profile = (EditText)findViewById(R.id.et_userid_profile);
        et_nama_profile = (EditText)findViewById(R.id.et_nama_profile);
        et_email_profile = (EditText)findViewById(R.id.et_email_profile);
        et_phone_profile = (EditText)findViewById(R.id.et_phone_profile);
        et_token_profile = (EditText)findViewById(R.id.et_token_profile);
        et_alamat_profile = (EditText)findViewById(R.id.et_alamat_profile);
        et_kota_profile = (EditText)findViewById(R.id.et_kota_profile);
        et_provinsi_profile = (EditText)findViewById(R.id.et_provinsi_profile);
        et_negara_profile = (EditText)findViewById(R.id.et_negara_profile);
        dialog = ProgressDialog.show(UpdateProfileActivity.this, "Profile Update",
                "Loading...", true);

        Intent intent = getIntent();
        idVendor = intent.getStringExtra("id");

        if (idVendor != null){
            et_nama_profile.setText(intent.getStringExtra("nama"));
            et_email_profile.setText(intent.getStringExtra("email"));
            et_phone_profile.setText(intent.getStringExtra("phone"));

            if (intent.getStringExtra("alamat") != null){
                et_alamat_profile.setText(intent.getStringExtra("alamat"));
            }
            if (intent.getStringExtra("kota") != null){
                et_kota_profile.setText(intent.getStringExtra("kota"));
            }
            if (intent.getStringExtra("provinsi") != null){
                et_provinsi_profile.setText(intent.getStringExtra("provinsi"));
            }
            if (intent.getStringExtra("negara") != null){
                et_negara_profile.setText(intent.getStringExtra("negara"));
            }
        }
        dialog.dismiss();

        btn_cancel_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

        btn_update_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                update();


            }
        });
    }

    public void update(){

        AplikasikuProgressDialog.show(this);
        String sVendorId = idVendor;
        String sUsername = et_nama_profile.getText().toString();
        String sEmail = et_email_profile.getText().toString();
        String sPhone = et_phone_profile.getText().toString();
        String sToken = et_token_profile.getText().toString();
        String sAlamat = et_alamat_profile.getText().toString();
        String sKota = et_kota_profile.getText().toString();
        String sProvinsi = et_provinsi_profile.getText().toString();
        String sNegara = et_negara_profile.getText().toString();

        ApiService update = Api.getInstance().createService(ApiService.class, EditVendorInfoDeserializer.converterFactory());
        JSONEditVendorInfo editVendorInfo = new JSONEditVendorInfo();

        editVendorInfo.setSurname(sUsername);
        editVendorInfo.setEmail(sEmail);
        editVendorInfo.setPhone(sPhone);
        editVendorInfo.setToken(sToken);
        editVendorInfo.setAddress1(sAlamat);
        editVendorInfo.setCity(sKota);
        editVendorInfo.setState(sProvinsi);
        editVendorInfo.setCountry(sNegara);
        editVendorInfo.setAddress2("");
        editVendorInfo.setFacebook("");
        editVendorInfo.setGooglePlus("");
        editVendorInfo.setOsVersion("");
        editVendorInfo.setUserId(sVendorId);
        editVendorInfo.setZip("");
        editVendorInfo.setSkype("");
        editVendorInfo.setHpType("");

        Call<ResponEditVendorInfo> vendorInfo = update.getEditVendorInfo(editVendorInfo);
        vendorInfo.enqueue(new Callback<ResponEditVendorInfo>() {
            @Override
            public void onResponse(Call<ResponEditVendorInfo> call, Response<ResponEditVendorInfo> response) {
                AplikasikuProgressDialog.dismiss();
                finish();
            }

            @Override
            public void onFailure(Call<ResponEditVendorInfo> call, Throwable t) {
                AplikasikuProgressDialog.dismiss();

            }
        });

    }
}
