package com.telkom.aplikasiku.main.register;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.telkom.aplikasiku.R;
import com.telkom.aplikasiku.api.Api;
import com.telkom.aplikasiku.api.ApiService;
import com.telkom.aplikasiku.deserialization.RegisterDeserializer;
import com.telkom.aplikasiku.main.home.MainActivity;
import com.telkom.aplikasiku.model.JSONRegister;
import com.telkom.aplikasiku.model.ModelRegister;
import com.telkom.aplikasiku.responses.ResponRegister;
import com.telkom.aplikasiku.utils.AplikasikuProgressDialog;
import com.telkom.aplikasiku.utils.AppUtils;
import com.telkom.aplikasiku.utils.SessionManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    Button button_reseller;
    EditText et_nama;
    EditText et_email;
    EditText et_password1;
    EditText et_password2;
    EditText et_hp;
    SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        session = new SessionManager(getApplicationContext());
        et_nama = (EditText)findViewById(R.id.ed_nama);
        et_email = (EditText)findViewById(R.id.ed_email);
        et_password1 = (EditText)findViewById(R.id.ed_password1);
        et_password2 = (EditText)findViewById(R.id.ed_password2);
        et_hp = (EditText)findViewById(R.id.ed_phone);


        button_reseller = (Button)findViewById(R.id.btn_reseller);
        button_reseller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_nama.getText().toString().isEmpty()){
                    et_nama.setError( "Name is required!" );
                }else if (et_email.getText().toString().isEmpty()){
                    et_email.setError( "Email is required!" );
                }else if (et_password1.getText().toString().isEmpty()){
                    //et_password1.setError( "Password is required!" );
                }else if (et_password2.getText().toString().isEmpty()){
                   // et_password2.setError( "Password is required!" );
                }else if (et_hp.getText().toString().isEmpty()){
                    et_hp.setError( "Phone Number is required!" );
                }else {
                    register();
                }


            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }

    private void register(){
        AplikasikuProgressDialog.show(this);
        String sUsername = et_nama.getText().toString();
        String sEmail = et_email.getText().toString();
        String sPassword1 = et_password1.getText().toString();
        String sPassword2 = et_password2.getText().toString();
        String sPhone = et_hp.getText().toString();
        String sOs_version = AppUtils.getAndroidVersion();
        String hp_type = AppUtils.getDeviceName();

        final ApiService register = Api.getInstance().createService(ApiService.class, RegisterDeserializer.converterFactory());
        JSONRegister jsonRegister = new JSONRegister();

        jsonRegister.setEmail(sEmail);
        jsonRegister.setPhone(sPhone);
        jsonRegister.setPassword1(sPassword1);
        jsonRegister.setPassword2(sPassword2);
        jsonRegister.setUsername(sUsername);
        jsonRegister.setOsVersion(sOs_version);
        jsonRegister.setHpType(hp_type);


        final Call<ResponRegister> regis = register.sendRegister(jsonRegister);
        regis.enqueue(new Callback<ResponRegister>() {
            @Override
            public void onResponse(Call<ResponRegister> call, Response<ResponRegister> response) {
                AplikasikuProgressDialog.dismiss();
                ModelRegister modelRegister = response.body().getMeta();
                session.createLoginSession(modelRegister.getUsername(), modelRegister.getEmail(), String.valueOf(modelRegister.getUserId()));

                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }

            @Override
            public void onFailure(Call<ResponRegister> call, Throwable t) {
                AplikasikuProgressDialog.dismiss();

            }
        });
    }
}
