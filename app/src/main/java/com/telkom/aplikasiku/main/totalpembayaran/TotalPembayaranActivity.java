package com.telkom.aplikasiku.main.totalpembayaran;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.telkom.aplikasiku.R;
import com.telkom.aplikasiku.main.datapembeli.DataPembeliActivity;

public class TotalPembayaranActivity extends AppCompatActivity {

    TextView tv_rp1;
    TextView tr_rp4;
    TextView tv_rp5;
    TextView tv_kontak;

    ImageView imageView;

    Context context;

    Button btn_pilih_pembayaran;
    LinearLayout layout;
    CheckBox checkBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_total_pembayaran);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar_totalPembayaran);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Keranjang");

        Intent intent = getIntent();
        final String id_produk = intent.getStringExtra("product_id");
        final  String harga = intent.getStringExtra("harga");

        tv_rp1 = (TextView)findViewById(R.id.tv_rp1);
        tr_rp4 = (TextView)findViewById(R.id.tr_rp4);
        tv_rp5 = (TextView)findViewById(R.id.tv_rp5);
        tv_kontak = (TextView)findViewById(R.id.tv_kontrak);
        imageView = (ImageView)findViewById(R.id.iv_pembelian);
        btn_pilih_pembayaran = (Button)findViewById(R.id.btn_pilih_pembayaran);
        layout = findViewById(R.id.layout);
        checkBox = findViewById(R.id.cb_kode_promo);

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b){
                    layout.setVisibility(View.VISIBLE);
                }else {
                    layout.setVisibility(View.GONE);
                }
            }
        });



        if (id_produk != null){
            tv_rp1.setText(intent.getStringExtra("harga"));
            tr_rp4.setText(intent.getStringExtra("harga"));
            tv_rp5.setText(intent.getStringExtra("harga"));
            tv_kontak.setText(intent.getStringExtra("title"));
            Picasso.with(context)
                    .load(intent.getStringExtra("image"))
                    .placeholder(R.color.textColor)
                    .into(imageView);

        }

        btn_pilih_pembayaran.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TotalPembayaranActivity.this, DataPembeliActivity.class);
                intent.putExtra("id", id_produk);
                intent.putExtra("harga", harga);
                startActivity(intent);
            }
        });

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }
}
