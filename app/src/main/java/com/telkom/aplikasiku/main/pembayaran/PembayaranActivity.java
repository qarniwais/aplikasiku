package com.telkom.aplikasiku.main.pembayaran;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.telkom.aplikasiku.R;
import com.telkom.aplikasiku.api.Api;
import com.telkom.aplikasiku.api.ApiService;
import com.telkom.aplikasiku.deserialization.PaymentDeserializer;
import com.telkom.aplikasiku.deserialization.RequestPaymentDeserializer;
import com.telkom.aplikasiku.main.success.SuccessActivity;
import com.telkom.aplikasiku.model.JSONRequestPayment;
import com.telkom.aplikasiku.responses.PaymentResponse;
import com.telkom.aplikasiku.responses.RequestPaymentResponse;
import com.telkom.aplikasiku.utils.AplikasikuProgressDialog;
import com.telkom.aplikasiku.utils.SessionManager;
import com.telkom.aplikasiku.utils.Utility;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PembayaranActivity extends AppCompatActivity {

    TextView tv_pembayaranselesai;
    Button selesai_bayar;
    TextView tvDesc;
    ImageView ivAtm, ivModern;
    PaymentResponse payment;
    SessionManager sessionManager;
    String email, nama, phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pembayaran);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbarPembayaran);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Aplikasiku");

        tvDesc = findViewById(R.id.tv_desc);
        ivAtm = findViewById(R.id.iv_atm);
        ivModern = findViewById(R.id.iv_modern);

        final Intent intent = getIntent();
        final String id_produk = intent.getStringExtra("id");
        final  String harga = intent.getStringExtra("harga");

        tv_pembayaranselesai = (TextView)findViewById(R.id.tv_pembayaranselesai);
        tv_pembayaranselesai.setText(intent.getStringExtra("harga"));
        selesai_bayar = (Button)findViewById(R.id.selesai_bayar);

        sessionManager = new SessionManager(this);
        HashMap<String, String> user = sessionManager.getDataPembeli();
        email = user.get(SessionManager.KEY_EMAIL_PEMBELI);
        nama = user.get(SessionManager.KEY_NAME_PEMBELI);
        phone = user.get(SessionManager.KEY_PHONE_PEMBELI);


        selesai_bayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONRequestPayment json = new JSONRequestPayment();
                json.setAmount(harga);
                json.setEmail(email);
                json.setName(nama);
                json.setInvoice(Utility.generateCode("INV"));
                requestPayment(json);
            }
        });

        getPayment();

        ivModern.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvDesc.setText(Html.fromHtml(payment.getIbank().getDescription()));
            }
        });

        ivAtm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvDesc.setText(Html.fromHtml(payment.getATM().getDescription()));
            }
        });
    }


    private void getPayment(){
        ApiService api = Api.getInstance().createServiceGeneral(ApiService.class, PaymentDeserializer.converterFactory());
        Call<PaymentResponse> userPayment = api.getPayment();
        userPayment.enqueue(new Callback<PaymentResponse>() {
            @Override
            public void onResponse(Call<PaymentResponse> call, Response<PaymentResponse> response) {
                payment = response.body();
                tvDesc.setText(Html.fromHtml(response.body().getATM().getDescription()));

                Glide.with(getApplicationContext()).load(response.body().getATM().getSourceImg()).into(ivAtm);
                Glide.with(getApplicationContext()).load(response.body().getIbank().getSourceImg()).into(ivModern);
            }

            @Override
            public void onFailure(Call<PaymentResponse> call, Throwable t) {
                System.out.println("fail");
            }
        });
    }

    private void requestPayment(JSONRequestPayment jsonRequestPayment){
        AplikasikuProgressDialog.show(this);
        ApiService api = Api.getInstance().createServiceGeneral(ApiService.class, RequestPaymentDeserializer.converterFactory());
        Call<RequestPaymentResponse> userPayment = api.requestPayment(jsonRequestPayment);
        userPayment.enqueue(new Callback<RequestPaymentResponse>() {
            @Override
            public void onResponse(Call<RequestPaymentResponse> call, Response<RequestPaymentResponse> response) {
                AplikasikuProgressDialog.dismiss();
                String invoice = response.body().getInvoice();
                String paycode = response.body().getPaycode();
                String status = response.body().getStatus();

                Intent intent = new Intent(PembayaranActivity.this, SuccessActivity.class);
                intent.putExtra("invoice", invoice);
                intent.putExtra("paycode", paycode);
                intent.putExtra("status", status);
                startActivity(intent);
                finish();
            }

            @Override
            public void onFailure(Call<RequestPaymentResponse> call, Throwable t) {
                System.out.println("fail");
                AplikasikuProgressDialog.dismiss();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }
}
