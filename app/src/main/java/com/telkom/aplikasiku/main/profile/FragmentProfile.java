package com.telkom.aplikasiku.main.profile;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.telkom.aplikasiku.R;
import com.telkom.aplikasiku.api.Api;
import com.telkom.aplikasiku.api.ApiService;
import com.telkom.aplikasiku.deserialization.VendorDetailDeserializer;
import com.telkom.aplikasiku.main.updateprofile.UpdateProfileActivity;
import com.telkom.aplikasiku.model.JSONVendorDetail;
import com.telkom.aplikasiku.model.ModelVendorDetail;
import com.telkom.aplikasiku.responses.ResponVendorDetail;
import com.telkom.aplikasiku.utils.AlertDialogManager;
import com.telkom.aplikasiku.utils.AplikasikuProgressDialog;
import com.telkom.aplikasiku.utils.AppUtils;
import com.telkom.aplikasiku.utils.NullHandling;
import com.telkom.aplikasiku.utils.SessionManager;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentProfile extends Fragment {

    AlertDialogManager dialogManager = new AlertDialogManager();

    SessionManager sessionManager;

    TextView tv_edit_data;
    TextView tv_logout;
    TextView tv_nama_user;
    TextView tv_email_user;
    TextView tv_alamat;
    TextView tv_no_handphone;
    TextView tv_kota;
    TextView tv_provinsi;
    TextView tv_negara;

    String nama;
    String email;
    String phone;
    String alamat, kota, provinsi, negara;

    Button btn_logout;

    String userId;
    ProgressDialog dialog;




    public FragmentProfile() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        dialog = ProgressDialog.show(getActivity(), "",
                "Loading...", true);

        sessionManager = new SessionManager(getActivity());
        tv_nama_user = (TextView)view.findViewById(R.id.tv_nama_user);
        tv_email_user = (TextView)view.findViewById(R.id.tv_email_user);
        tv_alamat = (TextView)view.findViewById(R.id.tv_alamat);
        tv_no_handphone = (TextView)view.findViewById(R.id.tv_no_handphone);
        tv_kota = (TextView)view.findViewById(R.id.tv_kota);
        tv_provinsi = (TextView)view.findViewById(R.id.tv_provinsi);
        tv_negara = (TextView)view.findViewById(R.id.tv_negara);

        sessionManager.checkLogin();

        userId = sessionManager.getUserId();

        HashMap<String, String> user = sessionManager.getUserDetails();

        tv_edit_data = (TextView)view.findViewById(R.id.tv_edit_data);
        tv_logout = (TextView)view.findViewById(R.id.tv_logout);

        tv_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sessionManager.logoutUser();
            }
        });

        tv_edit_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), UpdateProfileActivity.class);
                intent.putExtra("id", userId);
                intent.putExtra("nama",nama);
                intent.putExtra("email",email);
                intent.putExtra("phone",phone);
                intent.putExtra("alamat",alamat);
                intent.putExtra("kota",kota);
                intent.putExtra("provinsi",provinsi);
                intent.putExtra("negara",negara);
                startActivity(intent);

            }

        });
        dialog.dismiss();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getprofile();
    }

    private void getprofile() {
        AplikasikuProgressDialog.show(getActivity());
        ApiService api = Api.getInstance().createService(ApiService.class, VendorDetailDeserializer.converterFactory());
        JSONVendorDetail jsonVendor = new JSONVendorDetail();
        jsonVendor.setVendorId(userId);
        jsonVendor.setToken("");
        jsonVendor.setOsVersion(AppUtils.getAndroidVersion());
        jsonVendor.setHpType(AppUtils.getDeviceName());

        final Call<ResponVendorDetail> vendor = api.getVendorDetail(jsonVendor);

        vendor.enqueue(new Callback<ResponVendorDetail>() {
            @Override
            public void onResponse(Call<ResponVendorDetail> call, Response<ResponVendorDetail> response) {
                AplikasikuProgressDialog.dismiss();
                dialog = ProgressDialog.show(getActivity(), "Product",
                        "Loading...", true);

//                System.out.println("sukses = "+response.body().getData().toString());
                ModelVendorDetail mvendor = response.body().getData();


                System.out.println("sukses");
                System.out.println("vendor = "+mvendor.getEmail());

                if (mvendor.getSurname() == null) {
                    nama = mvendor.getUsername();
                    tv_nama_user.setText(mvendor.getUsername());
                }else {
                    tv_nama_user.setText(mvendor.getSurname());
                    nama = mvendor.getSurname();
                }
                tv_email_user.setText(mvendor.getEmail());
                tv_alamat.setText(NullHandling.parse(mvendor.getAddress1()));
                tv_no_handphone.setText(NullHandling.parse(mvendor.getPhone()));
                tv_kota.setText(NullHandling.parse(mvendor.getCity()));
                tv_provinsi.setText(NullHandling.parse(mvendor.getState()));
                tv_negara.setText(NullHandling.parse(mvendor.getCountry()));

                email = mvendor.getEmail();
                phone = mvendor.getPhone();
                alamat = NullHandling.parse(mvendor.getAddress1());
                kota = NullHandling.parse(mvendor.getCity());
                provinsi = NullHandling.parse(mvendor.getState());
                negara = NullHandling.parse(mvendor.getCountry());

                dialog.dismiss();


            }

            @Override
            public void onFailure(Call<ResponVendorDetail> call, Throwable t) {
                System.out.println("fail");
                t.printStackTrace();
                AplikasikuProgressDialog.dismiss();

            }
        });
    }


}
