package com.telkom.aplikasiku.main.success;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.telkom.aplikasiku.R;
import com.telkom.aplikasiku.main.home.MainActivity;

public class SuccessActivity extends AppCompatActivity {

    Button btn_backtohome;
    TextView tvInvoice, tvPaycode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        tvInvoice = findViewById(R.id.tv_invoice);
        tvPaycode = findViewById(R.id.tv_paycode);

        Intent intent = getIntent();
        tvInvoice.setText(intent.getStringExtra("invoice"));
        tvPaycode.setText(intent.getStringExtra("paycode"));



        btn_backtohome = (Button)findViewById(R.id.btn_backtohome);
        btn_backtohome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SuccessActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });

    }
}
