package com.telkom.aplikasiku.main.halamanutama;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.telkom.aplikasiku.R;
import com.telkom.aplikasiku.adapter.AdapterHalamanUtama;
import com.telkom.aplikasiku.main.faq.FaqActivity;


public class FragmentHalamanUtama extends Fragment {
    private ViewPager pager;
    private int[] layouts = {R.layout.pager1, R.layout.pager2};
    private AdapterHalamanUtama adapter;

    ImageButton image_button_faq;
    ImageButton ib_fitur_reseller;
    ImageButton ib_benefit;


    public FragmentHalamanUtama() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_halaman_utama, container, false);

        image_button_faq = (ImageButton)view.findViewById(R.id.img_btnfaq);
        ib_fitur_reseller = (ImageButton)view.findViewById(R.id.ib_fitur_reseller);
        ib_benefit = (ImageButton)view.findViewById(R.id.ib_benefit);

        ib_benefit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(getActivity());
                // Include dialog.xml file
                dialog.setContentView(R.layout.benefit);

                TextView text = (TextView) dialog.findViewById(R.id.tv_benefit_text);
                text.setText(R.string.benefit);
                ImageView image = (ImageView) dialog.findViewById(R.id.iv_benefitImage);
                image.setImageResource(R.drawable.benefit);

                dialog.show();


            }
        });

        ib_fitur_reseller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(getActivity());
                // Include dialog.xml file
                dialog.setContentView(R.layout.reseller);

                TextView text = (TextView) dialog.findViewById(R.id.tv_reseller_text);
                text.setText(R.string.reseller);
                ImageView image = (ImageView) dialog.findViewById(R.id.iv_resellerImage);
                image.setImageResource(R.mipmap.img_banner2);

                dialog.show();
            }
        });

        image_button_faq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), FaqActivity.class);
                startActivity(intent);
            }
        });


        pager = (ViewPager)view.findViewById(R.id.viewPager);
        adapter = new AdapterHalamanUtama(layouts, getActivity());
        pager.setAdapter(adapter);

        return view;
    }

}
