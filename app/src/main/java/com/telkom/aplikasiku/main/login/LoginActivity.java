package com.telkom.aplikasiku.main.login;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.telkom.aplikasiku.R;
import com.telkom.aplikasiku.api.Api;
import com.telkom.aplikasiku.api.ApiService;
import com.telkom.aplikasiku.deserialization.ChangeVendorPasswordDeserializer;
import com.telkom.aplikasiku.deserialization.LoginDeserializer;
import com.telkom.aplikasiku.deserialization.RegisterDeserializer;
import com.telkom.aplikasiku.main.home.MainActivity;
import com.telkom.aplikasiku.main.register.RegisterActivity;
import com.telkom.aplikasiku.model.JSONLogin;
import com.telkom.aplikasiku.model.ModelLogin;
import com.telkom.aplikasiku.responses.LoginResponse;
import com.telkom.aplikasiku.utils.AlertDialogManager;
import com.telkom.aplikasiku.utils.AplikasikuProgressDialog;
import com.telkom.aplikasiku.utils.AppUtils;
import com.telkom.aplikasiku.utils.SessionManager;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    Button button_masuk_login;
    EditText et_email;
    EditText et_password;

    TextView tv_lupa_sandi;
    TextView tv_daftar;

    EditText password;
    EditText passwordBaru;
    EditText passwordBaru1;

    final Context mContext = this;

    AlertDialogManager alert = new AlertDialogManager();

    SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        session = new SessionManager(getApplicationContext());

        et_email = (EditText)findViewById(R.id.et_email_login);
        et_password = (EditText)findViewById(R.id.et_pass_login);
        button_masuk_login = (Button)findViewById(R.id.btn_masuk_login);
        tv_lupa_sandi = (TextView)findViewById(R.id.tv_lupa_sandi);
        tv_daftar = (TextView)findViewById(R.id.tv_daftar);

        tv_lupa_sandi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater li = LayoutInflater.from(mContext);
                View dialogView = li.inflate(R.layout.layout_lupa_sandi, null);
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder (mContext);
                // set title
                alertDialogBuilder.setTitle("New Password");
                // set custom dialog icon
                //alertDialogBuilder.setIcon(R.drawable.ic_launcher_background);
                // set custom_dialog.xml to alertdialog builder
                alertDialogBuilder.setView(dialogView);
                final EditText password = (EditText) dialogView.findViewById(R.id.et_password);
                final EditText passwordBaru = (EditText) dialogView.findViewById(R.id.et_password1);
                final EditText passwordBaru1 = (EditText) dialogView.findViewById(R.id.et_password2);
                // set dialog message
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                        String sUsername = password.getText().toString();
                                        String sEmail = passwordBaru.getText().toString();
                                        String sPassword1 = passwordBaru1.getText().toString();

                                        //final ApiService register = Api.getInstance().createService(ApiService.class, ChangeVendorPasswordDeserializer.converterFactory());
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();
                // show it
                alertDialog.show();
            }
        });

        tv_daftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

        Toast.makeText(LoginActivity.this, "User Login Status : "+session.isLoggedIn(), Toast.LENGTH_SHORT).show();

        button_masuk_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }

        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }

    private void login(){
        AplikasikuProgressDialog.show(this);

        String username = et_email.getText().toString();
        String password = et_password.getText().toString();

        ApiService api = Api.getInstance().createService(ApiService.class,LoginDeserializer.converterFactory());
        JSONLogin login = new JSONLogin();
        login.setEmail(username);
        login.setPassword(password);
        login.setOsVersion(AppUtils.getAndroidVersion());
        login.setHpType(AppUtils.getDeviceName());


        Call<LoginResponse> userLogin = api.login(login);
        userLogin.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                AplikasikuProgressDialog.dismiss();
                try {
                    System.out.println("sukses" + response);
                    ModelLogin mLogin = response.body().getLogin();
                    mLogin.getUser_id();
                    session.createLoginSession(mLogin.getUsername(), mLogin.getEmail(), mLogin.getUser_id());

                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    //finish();
                }catch (Exception e){
            }

        }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                AplikasikuProgressDialog.dismiss();
                System.out.println("fail");
            }
        });
    }
}
