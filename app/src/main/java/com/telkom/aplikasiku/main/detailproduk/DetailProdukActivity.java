package com.telkom.aplikasiku.main.detailproduk;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.telkom.aplikasiku.R;
import com.telkom.aplikasiku.api.Api;
import com.telkom.aplikasiku.api.ApiService;
import com.telkom.aplikasiku.deserialization.DetailProductDeserialzer;
import com.telkom.aplikasiku.deserialization.LoginDeserializer;
import com.telkom.aplikasiku.main.sosialmedia.SosialMediaActivity;
import com.telkom.aplikasiku.main.totalpembayaran.TotalPembayaranActivity;
import com.telkom.aplikasiku.model.JSONDetailProduct;
import com.telkom.aplikasiku.model.ModelProductDetail;
import com.telkom.aplikasiku.model.ModelReferalUser;
import com.telkom.aplikasiku.responses.ResponseProductProfile;
import com.telkom.aplikasiku.utils.AplikasikuProgressDialog;
import com.telkom.aplikasiku.utils.AppUtils;
import com.telkom.aplikasiku.utils.SessionManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailProdukActivity extends AppCompatActivity {

    Button button_bagi_uang;
    Button button_membagikan;

    TextView tv_title;
    TextView tv_harga;
    RatingBar tv_rating;
    TextView tv_deskripsi;

    ImageView imageView;

    Context context;

    String id_produk;
    String harga;
    String title;
    String image;

    String shareLink;

    ProgressDialog dialog;

    SessionManager session;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_produk);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar_detailProduk);
        setSupportActionBar(toolbar);

        session = new SessionManager(this);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        Intent intent = getIntent();

        id_produk = intent.getStringExtra("product_id");
        harga  = intent.getStringExtra("harga");
        title  = intent.getStringExtra("title");

        tv_title = (TextView)findViewById(R.id.tv_title_detail);
        tv_harga = (TextView)findViewById(R.id.tv_price_detail);
        tv_rating = (RatingBar)findViewById(R.id.ratingbar_detail);
        tv_deskripsi = (TextView)findViewById(R.id.tv_deskripsi_detail);
        button_bagi_uang = (Button)findViewById(R.id.btn_bagikanuang);
        button_membagikan = (Button)findViewById(R.id.btn_membagikan);
        imageView = (ImageView)findViewById(R.id.iv_detail_image);

        button_bagi_uang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent shareIntent = new Intent(Intent.ACTION_SEND);

                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareLink);
                startActivity(Intent.createChooser(shareIntent, "Share using"));
            }
        });

        button_membagikan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(DetailProdukActivity.this, TotalPembayaranActivity.class);
                intent1.putExtra("product_id", id_produk);
                intent1.putExtra("harga", harga);
                intent1.putExtra("title", title);
                intent1.putExtra("image", image );
                startActivity(intent1);
            }
        });

        if (id_produk != null){
//            tv_title.setText(intent.getStringExtra("title"));
//            tv_harga.setText(intent.getStringExtra("harga"));
//            tv_rating.setRating(Float.parseFloat(intent.getStringExtra("rating")));
//            tv_deskripsi.setText(Html.fromHtml(intent.getStringExtra("deskripsi")));
//            Picasso.with(context)
//                    .load(intent.getStringExtra("image"))
//                    .placeholder(R.color.colorPrimary)
//                    .into(imageView);
        }

        getDetailProduct();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
//            case R.id.cart:
//                return true;
            default:
                onBackPressed();

        }
        return super.onOptionsItemSelected(item);
    }

    private void getDetailProduct(){
        AplikasikuProgressDialog.show(this);
        ApiService api = Api.getInstance().createService(ApiService.class, DetailProductDeserialzer.converterFactory());

        JSONDetailProduct param = new JSONDetailProduct();
        param.setUserId(session.getUserId());
        param.setProductId(id_produk);
        param.setToken("");
        param.setOsVersion(AppUtils.getAndroidVersion());
        param.setHpType(AppUtils.getDeviceName());

        Call<ResponseProductProfile> call = api.getDetailProduct(param);
        call.enqueue(new Callback<ResponseProductProfile>() {
            @Override
            public void onResponse(Call<ResponseProductProfile> call, Response<ResponseProductProfile> response) {
                AplikasikuProgressDialog.dismiss();
                System.out.println("response = "+response.body().getData().getProductDetails().get(0).getProductId());

                ModelReferalUser refUser = response.body().getData().getUser();
                ModelProductDetail detailProduct = response.body().getData().getProductDetails().get(0);

                tv_title.setText(detailProduct.getTitle());
                tv_harga.setText(harga);
                tv_rating.setRating(Float.parseFloat(detailProduct.getRatingNum()));
                tv_deskripsi.setText(Html.fromHtml(detailProduct.getDescription()));
                image = "https://asset.aplikasiku.id/product_image/"+detailProduct.getMainPhotos();
                Picasso.with(context)
                        .load(image)
                        .placeholder(R.color.colorPrimary)
                        .into(imageView);

                shareLink = refUser.getReferralUrl();
            }

            @Override
            public void onFailure(Call<ResponseProductProfile> call, Throwable t) {
                AplikasikuProgressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }
}
