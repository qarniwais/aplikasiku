package com.telkom.aplikasiku.main.datapembeli;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.telkom.aplikasiku.R;
import com.telkom.aplikasiku.main.home.MainActivity;
import com.telkom.aplikasiku.main.pembayaran.PembayaranActivity;
import com.telkom.aplikasiku.main.success.SuccessActivity;
import com.telkom.aplikasiku.main.sukses.SuksesActivity;
import com.telkom.aplikasiku.utils.SessionManager;

public class DataPembeliActivity extends AppCompatActivity {

    EditText et_nama_pembeli;
    EditText et_email_pembeli;
    EditText et_phone_pembeli;
    Button btn_simpan_pembeli;

    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_pembeli);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar_data_pembeli);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        final String id_produk = intent.getStringExtra("id");
        final  String harga = intent.getStringExtra("harga");

        sessionManager = new SessionManager(this);

        et_nama_pembeli = (EditText)findViewById(R.id.et_nama_pembeli);
        et_email_pembeli = (EditText)findViewById(R.id.et_email_pembeli);
        et_phone_pembeli = (EditText)findViewById(R.id.et_phone_pembeli);
        btn_simpan_pembeli = (Button)findViewById(R.id.btn_simpan_pembeli);

        btn_simpan_pembeli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_nama_pembeli.getText().toString().isEmpty()){
                et_nama_pembeli.setError( "Name is required!" );
            }else if (et_email_pembeli.getText().toString().isEmpty()){
                et_email_pembeli.setError( "Email is required!" );
            }else {
                saveData(et_nama_pembeli.getText().toString(),et_email_pembeli.getText().toString(),et_phone_pembeli.getText().toString());
                Intent intent = new Intent(DataPembeliActivity.this, PembayaranActivity.class);
                intent.putExtra("id", id_produk);
                intent.putExtra("harga", harga);
                startActivity(intent);
                }
            }
        });

    }

    private void saveData(String nama, String email, String phone){


        sessionManager.dataPembeli(nama, email, phone);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }
}
