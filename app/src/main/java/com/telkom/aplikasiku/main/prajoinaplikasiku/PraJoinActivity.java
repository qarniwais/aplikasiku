package com.telkom.aplikasiku.main.prajoinaplikasiku;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.telkom.aplikasiku.R;
import com.telkom.aplikasiku.main.login.LoginActivity;
import com.telkom.aplikasiku.main.register.RegisterActivity;

public class PraJoinActivity extends AppCompatActivity {
    Button button_daftar, button_masuk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pra_join);

        button_daftar = (Button)findViewById(R.id.btn_daftar);
        button_daftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PraJoinActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

        button_masuk = (Button)findViewById(R.id.btn_masuk);
        button_masuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PraJoinActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });


    }
}
