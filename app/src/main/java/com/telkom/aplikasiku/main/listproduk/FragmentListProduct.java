package com.telkom.aplikasiku.main.listproduk;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;

import com.telkom.aplikasiku.R;
import com.telkom.aplikasiku.adapter.AdapterListProduct;
import com.telkom.aplikasiku.adapter.AdapterListProductL;
import com.telkom.aplikasiku.api.ApiService;
import com.telkom.aplikasiku.api.ServerData;
import com.telkom.aplikasiku.model.ModelListProduk;
import com.telkom.aplikasiku.responses.ResponListProduct;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FragmentListProduct extends Fragment {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter dataAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private List<ModelListProduk> list = new ArrayList<>();

    ProgressDialog dialog;

    ImageButton btn_grid;
    ImageButton btn_list;


    public FragmentListProduct() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_list_product, container, false);

        Spinner spinner = (Spinner)view.findViewById(R.id.spinner1);
        String[] items = new String[]{"Terbaru","Terlama"};

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, items);
        spinner.setAdapter(adapter);

        btn_grid = (ImageButton)view.findViewById(R.id.grid);
        btn_list = (ImageButton)view.findViewById(R.id.list);

        btn_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                recyclerView  = (RecyclerView)view.findViewById(R.id.recycler_view);
                layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                recyclerView.setLayoutManager(layoutManager);

                ApiService requestData = ServerData.getClient().create(ApiService.class);


                final Call<ResponListProduct> modelCall = requestData.getData();
                dialog = ProgressDialog.show(getActivity(), "",
                        "Loading...", true);

                modelCall.enqueue(new Callback<ResponListProduct>() {


                    @Override
                    public void onResponse(Call<ResponListProduct> call, Response<ResponListProduct> response) {

                        list = response.body().getData();
                        dataAdapter = new AdapterListProductL(getActivity(), list);
                        recyclerView.setAdapter(dataAdapter);
                        dataAdapter.notifyDataSetChanged();

                        dialog.dismiss();
                    }


                    @Override
                    public void onFailure(Call<ResponListProduct> call, Throwable t) {

                    }

                });
            }
        });

        btn_grid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerView  = (RecyclerView)view.findViewById(R.id.recycler_view);
                layoutManager = new GridLayoutManager(getActivity(),2, GridLayoutManager.VERTICAL, false);
                recyclerView.setLayoutManager(layoutManager);

                ApiService requestData = ServerData.getClient().create(ApiService.class);


                final Call<ResponListProduct> modelCall = requestData.getData();
                dialog = ProgressDialog.show(getActivity(), "",
                        "Loading...", true);

                modelCall.enqueue(new Callback<ResponListProduct>() {


                    @Override
                    public void onResponse(Call<ResponListProduct> call, Response<ResponListProduct> response) {

                        list = response.body().getData();
                        dataAdapter = new AdapterListProduct(getActivity(), list);
                        recyclerView.setAdapter(dataAdapter);
                        dataAdapter.notifyDataSetChanged();

                        dialog.dismiss();
                    }


                    @Override
                    public void onFailure(Call<ResponListProduct> call, Throwable t) {

                    }

                });
            }
        });

        recyclerView  = (RecyclerView)view.findViewById(R.id.recycler_view);
        layoutManager = new GridLayoutManager(getActivity(),2, GridLayoutManager.VERTICAL, false);

        recyclerView.setLayoutManager(layoutManager);

        ApiService requestData = ServerData.getClient().create(ApiService.class);


        final Call<ResponListProduct> modelCall = requestData.getData();
        dialog = ProgressDialog.show(getActivity(), "",
                "Loading...", true);

        modelCall.enqueue(new Callback<ResponListProduct>() {


            @Override
            public void onResponse(Call<ResponListProduct> call, Response<ResponListProduct> response) {

                list = response.body().getData();
                dataAdapter = new AdapterListProduct(getActivity(), list);
                recyclerView.setAdapter(dataAdapter);
                dataAdapter.notifyDataSetChanged();

                dialog.dismiss();
            }


            @Override
            public void onFailure(Call<ResponListProduct> call, Throwable t) {

            }

        });




        return view;

    }


}
